/*
 * main.cxx
 * Created on: 24/08/2012
 * Author: heekinho
 */

#include "load_prc_file.h"
#include "pandaFramework.h"

#include "fadeLodNode.h"
#include "shaderPool.h"

//#include "pagedGeometry.h"
//#include "treeLoader3d.h"
//#include "batchPage.h"

#include "impostorManager.h"
#include "impostorMaker.h"
#include "impostorPage.h"


PandaFramework framework;
WindowFramework* window;

vector<string> vegetables_paths;
vector<NodePath> vegetables_models;

/* Load the vegetables to use in the impostor system */
void load_vegetables(){
	vegetables_paths.push_back("vegetation/Bocoa/bocoa.egg");
	vegetables_paths.push_back("vegetation/Bromelia/bromelia");
	vegetables_paths.push_back("vegetation/Chamaecrista/chamaecrista");
	vegetables_paths.push_back("vegetation/Colher/colher");
	vegetables_paths.push_back("vegetation/Copaifera/copaifera_model");
	vegetables_paths.push_back("vegetation/Croton/croton_model");
	vegetables_paths.push_back("vegetation/Eugenia/eugenia_model");
	vegetables_paths.push_back("vegetation/Harpochilus/harpochilus_model");
	vegetables_paths.push_back("vegetation/Jatropha/jatropha_model");
	vegetables_paths.push_back("vegetation/Mandacaru/mandacaru");
	vegetables_paths.push_back("vegetation/Mimosa/mimosa");
	vegetables_paths.push_back("vegetation/Murici/murici_rasteiro");
	vegetables_paths.push_back("vegetation/Quipa/quipa_model");
	vegetables_paths.push_back("vegetation/Simaba/simaba_model");
	vegetables_paths.push_back("vegetation/Xique_xique/xique_model");

	for(unsigned int i = 0; i < vegetables_paths.size(); ++i){
		NodePath vegetable = window->load_model(framework.get_models(), vegetables_paths[i]);
		vegetable.set_scale(0.05);
		vegetable.flatten_light();
		vegetables_models.push_back(vegetable);
	}
}

/* Models need to be tagged for use in the system */
void tag_vegetables(){
	for(unsigned int i = 0; i < vegetables_models.size(); ++i){
		NodePath vegetable = vegetables_models[i];
		vegetable.set_tag("Impostor.name", vegetables_paths[i]);
	}
}

/* Only need to be generated once... afterwards we load from disk */
void make_simdunas_vegetable_impostors(){
	ImpostorMaker maker;
	PT(SpriteSheet) spritesheet = new SpriteSheet(4096, 4096, 16, 16, 4);
	PT(ImpostorManager) manager = new ImpostorManager();
	manager->set_spritesheet(spritesheet);

	for(unsigned int i = 0; i < vegetables_models.size(); ++i){
		NodePath vegetable = vegetables_models[i];
		PT(Impostor) impostor = maker.create_impostor(window->get_graphics_window(), manager->get_spritesheet(), i, vegetable);
		manager->add_impostor(vegetables_paths[i], impostor);
	}

	nout << "Filtering Image..." << endl;
	maker.filter_binary_image(spritesheet->image, 8);

	nout << "Saving Impostors" << endl;
	manager->save_to_disk("forest.txt", "forest.png");
}



void impostor_sample(){
	/* Get the root of scene graph (render) and the default camera */
	NodePath render = window->get_render();
	NodePath camera = window->get_camera_group().get_child(0);

	srand(100);
	load_vegetables();
	tag_vegetables();
//	make_simdunas_vegetable_impostors();

	PT(ImpostorManager) manager = new ImpostorManager();
	manager->load("forest.txt");

	int qtd_vegetables = 1000;
	NodePath vegetables = NodePath("Vegetables");
	vegetables.reparent_to(render);
	for(int i = 0; i < qtd_vegetables; i++){
		int chosen = rand() % vegetables_models.size();

		NodePath vegetable = NodePath("vegetable instance");
		vegetables_models[chosen].instance_to(vegetable);
		vegetable.reparent_to(vegetables);

		vegetable.set_h(rand() % 360);
		vegetable.set_x(rand() % 250 * 4);
		vegetable.set_y(rand() % 250 * 4);
	}

	PT(ImpostorPage) page1 = new ImpostorPage(qtd_vegetables, render, manager);
	page1->generate(vegetables.get_children());

	PT(LODNode) lod_node = new LODNode("vegetal batch");
//	lod_node->set_fade_time(0.5f);
	NodePath lod_np = NodePath(lod_node);
	lod_np.reparent_to(render);

	lod_node->add_switch(500.0, 0.0);
	vegetables.reparent_to(lod_np);

	lod_node->add_switch(9999.0, 500.0);
	page1->get_impostors().reparent_to(lod_np);


	/* Main Loop */
	while(framework.do_frame(Thread::get_current_thread())){

	}
}



#include "auto_bind.h"
#include "boundingBox.h"

void animated_pandas_sample(){
	AnimControlCollection anim_control;
	NodePath spider = window->load_model(window->get_render(), "panda-model");
	NodePath pandaanim = window->load_model(spider, "panda-walk4");

	LPoint3f spmin, spmax;
	spider.calc_tight_bounds(spmin, spmax);
	spider.set_scale(1.0 / spmax[1]);
	spider.flatten_strong();

	auto_bind(spider.node(), anim_control, 63);

	nout << anim_control.get_anim(0)->get_num_frames() << endl;
	/* Animated Billboard Test */
	ImpostorMaker maker;
	PT(SpriteSheet) spritesheet = new SpriteSheet(2048, 2048, 64, 32, 4);

	PT(Impostor) impostor = maker.make_animated(window->get_graphics_window(), spritesheet, spider, anim_control.get_anim(0), "character");

	anim_control.loop_all(false);



	NodePath render = window->get_render();
	NodePath camera = window->get_camera_group().get_child(0);


	LVector2f scene_size (256, 256);

	PT(ImpostorManager) manager = new ImpostorManager();
	manager->set_spritesheet(spritesheet);
	manager->add_impostor("panda", impostor);

	int qtd_pandas = 400;
	NodePath page1 = render.attach_new_node("Page 01");
	PT(ImpostorPage) page = new ImpostorPage(qtd_pandas, page1, manager);

	/* Initialize the inputs */
	LVector3f up = render.get_relative_vector(camera, LVector3f(0, 0, 1));
	LVector3f right = render.get_relative_vector(camera, LVector3f(1, 0, 0));
	LVector3f forward = render.get_relative_vector(camera, LVector3f(0, 1, 0));
	page1.set_shader_input("up", LVector4f(up[0], up[1], up[2], 1));
	page1.set_shader_input("right", LVector4f(right[0], right[1], right[2], 1));
	page1.set_shader_input("forward", LVector4f(forward[0], forward[1], forward[2], 1));
	page1.set_shader_input("frame", LVector4f(0));

//	page1.set_texture(thebill.texture);
//	page1.set_transparency(TransparencyAttrib::M_binary);

	NodePath mypandas = NodePath("pandas");// render.attach_new_node("pandas");
	for(int i = 0; i < qtd_pandas; i++){
		NodePath temp = NodePath("dummy");
		temp.set_tag("Impostor.name", "panda");
		temp.set_pos(rand() % int(scene_size[0]), rand() % int(scene_size[1]), 0);
		temp.set_h(rand() % 360);
		temp.reparent_to(mypandas);
	}
	page->generate(mypandas.get_children());

	page1.set_shader(ShaderPool::load_shader("billboard-animated.sha"), 2);

	page1.node()->set_bounds(new BoundingBox(LPoint3f(0, 0, -1), LPoint3f(scene_size[0]+2, scene_size[1]+2, 50)));
	page1.node()->set_final(true);

//	make_float_colors(generatorNode);


//	generatorNode.show_bounds();
	while (framework.do_frame(Thread::get_current_thread())) {
		/* Recalculate the camera vectors every frame. =D */
		up = render.get_relative_vector(camera, LVector3f(0, 0, 1));
		right = render.get_relative_vector(camera, LVector3f(1, 0, 0));
		forward = render.get_relative_vector(camera, LVector3f(0, 1, 0));
		page1.set_shader_input("up", LVector4f(up[0], up[1], up[2], 1));
		page1.set_shader_input("right", LVector4f(right[0], right[1], right[2], 1));
		page1.set_shader_input("forward", LVector4f(forward[0], forward[1], forward[2], 1));
		page1.set_shader_input("frame", LVector4f(anim_control.get_anim(0)->get_frame()));

//		nout << anim_control.get_anim(0)->get_frame() << endl;
	}

}




#include "pagedGeometry.h"
#include "treeLoader3d.h"
#include "batchPage.h"
#include "cardMaker.h"


class MyTreeLoader : public PageLoader {
public:
	/*! Função auxiliar */
	float random(float lower, float higher){
		return lower + (higher-lower)*(float(rand())/RAND_MAX);
	}


	MyTreeLoader(){}
	virtual ~MyTreeLoader(){}

	virtual void loadPage(PageInfo &page){
		if(debug_page_cards.find(page.centerPoint) == debug_page_cards.end()){
			CardMaker cm ("Cell");
			cm.set_frame(LVertex(page.bounds.left, page.bounds.bottom, 0),
						 LVertex(page.bounds.right, page.bounds.bottom, 0),
						 LVertex(page.bounds.right, page.bounds.top, 0),
						 LVertex(page.bounds.left, page.bounds.top, 0));
			cm.set_color(rand()/float(RAND_MAX), 0, 0, 1);
			NodePath cmnp = NodePath(cm.generate());
			cmnp.set_two_sided(true);
			debug_page_cards[page.centerPoint] = cmnp;
		}

		if(trees_group.find(page.centerPoint) == trees_group.end()){
			trees_group[page.centerPoint] = NodePath("Group of Trees");

			for(int i = 0; i < 4; i++){
				NodePath placeholder = trees_group[page.centerPoint].attach_new_node("Tree Placeholder");
				trees[0].instance_to(placeholder);
				placeholder.set_x(random(page.bounds.left, page.bounds.right));
				placeholder.set_y(random(page.bounds.bottom, page.bounds.top));
				placeholder.set_h(placeholder, random(0, 360));
				placeholder.set_scale(random(0.7, 1.5));
			}
		}

		addEntity(trees_group[page.centerPoint]);
		addEntity(debug_page_cards[page.centerPoint]);
	}
	virtual void unloadPage(PageInfo &page) {}
	virtual void frameUpdate() {}

	void addTree(NodePath tree){
		trees.push_back(tree);
	}
protected:
	pmap<LPoint3f, NodePath> debug_page_cards;
	pmap<LPoint3f, NodePath> trees_group;
	pvector<NodePath> trees;

};


#include "multiInstanceManager.h"
#include "instanceNode.h"

class MyHWInstancePage : public BatchPage {
public:
//		class MyHWInstancePageData : public ReferenceCount {
//		public:
//			MyHWInstancePageData(PT(MultiInstanceManager) instance_manager, int vegetable_budget){
//				MyHWInstancePageData::instance_manager = instance_manager;
//				MyHWInstancePageData::vegetable_budget = vegetable_budget;
//			}
//
//			PT(MultiInstanceManager) instance_manager;
//			int vegetable_budget;
//		};

	PT(MultiInstanceManager) instance_manager;
//	pmap<NodePath, NodePath> instance_references;


//	PT(MyHWInstancePageData) data;
//	PT(ImpostorPage) page;
	bool is_built;

//	NodePath myinstances;

	MyHWInstancePage(){
		nout << "Construction MyHWInstancePage" << endl;
		is_built = false;
//		myinstances = NodePath("My Instances");
		instance_manager = new MultiInstanceManager(8);
	}

	void init(PagedGeometry *_geom, void* vdata){
		BatchPage::init(_geom, NULL);
//		this->data = (MyHWInstancePageData*) vdata;

//		nout << "The Vegetal Bugdet is: " << data->vegetable_budget << endl;
		//page = new ImpostorPage(data->vegetable_budget, batch, data->impostor_manager);
	}

	void build(){
		if(!is_built){
//			nout << "Generating the Impostors" << endl;
//			page->generate(batch.get_children());

			instance_manager->update();
			is_built = true;
		}

//		page->get_impostors().reparent_to(geom->getSceneNode());
		batch.reparent_to(geom->getSceneNode());
	}

	pmap<string, NodePath> registered;
	NodePath dummy;

	void addEntity(NodePath &entity){
		nout << "Adding Instance" << endl;
//		entity.instance_to(batch);
//		myinstances.attach_new_node(data->instance_manager->make_instance(entity));

		// Just add if not built yet (temporary)
		if(!is_built){

			NodePathCollection tagged_nodes = entity.find_all_matches("**/=Impostor.name");
			for(int i = 0; i < tagged_nodes.size(); i++){
				NodePath node = tagged_nodes[i];
				string name = node.get_tag("Impostor.name");


				if(registered.find(name) == registered.end()){
					NodePath instanced_tree = batch.attach_new_node("Instanced Tree");
					node.copy_to(instanced_tree);
					registered[name] = instanced_tree;
					instance_manager->register_model(instanced_tree, 4, true, true);
				}

				NodePath instance = dummy.attach_new_node(instance_manager->make_instance(registered[name]));
				instance.set_transform(node.get_net_transform());
			}

		}

	}

	void removeEntities(){
//		if(page) page->get_impostors().detach_node();
		batch.detach_node();

//		batch.remove_node();
//		batch = NodePath("batch");
	}

	void setVisible(bool visible){
		if(visible) batch.unstash();//batch.show();
		else batch.stash();//batch.hide();
	}
};







class MyTreePage : public BatchPage {
public:
	/** As this class is a callback, all data should be passed as *void in the init.
	 *  So it's good to separate the needed data */
	class MyTreePageData : public ReferenceCount {
	public:
		MyTreePageData(PT(ImpostorManager) impostor_manager, int vegetable_budget){
			this->impostor_manager = impostor_manager;
			this->vegetable_budget = vegetable_budget;
		}

		PT(ImpostorManager) impostor_manager;
		int vegetable_budget;
	};

protected:
	PT(MyTreePageData) data;
	PT(ImpostorPage) page;
	bool is_built;

public:
	MyTreePage(){
		is_built = false;
		nout << "Construction MyTreePage" << endl;
	}

	void init(PagedGeometry *_geom, void* vdata){
		BatchPage::init(_geom, NULL);
		this->data = (MyTreePageData*) vdata;

		nout << "The Vegetal Bugdet is: " << data->vegetable_budget << endl;
		page = new ImpostorPage(data->vegetable_budget, batch, data->impostor_manager);
	}

	void build(){
		if(!is_built){
			nout << "Generating the Impostors" << endl;
			page->generate(batch.get_children());
			is_built = true;
		}

		page->get_impostors().reparent_to(geom->getSceneNode());
		//batch.reparent_to(geom->getSceneNode());
	}

	void addEntity(NodePath &entity){
		nout << "Adding Entity" << endl;
		if(!is_built){
			entity.instance_to(batch);
		}
	}

	void removeEntities(){
		if(page) page->get_impostors().detach_node();
//		batch.detach_node();

//		batch.remove_node();
//		batch = NodePath("batch");
	}

	void setVisible(bool visible){
		if(visible) page->get_impostors().unstash(); //batch.unstash();//batch.show();
		else page->get_impostors().stash();//batch.stash();//batch.hide();
	}
};




void paged_geometry_sample(){
	/* Get the root of scene graph (render) and the default camera */
	NodePath render = window->get_render();
	NodePath camera = window->get_camera_group().get_child(0);



	srand(100);
	load_vegetables();
	tag_vegetables();
//	make_simdunas_vegetable_impostors();

	PT(ImpostorManager) manager = new ImpostorManager();
	manager->load("forest.txt");

//	int qtd_vegetables = 1000;
//	NodePath vegetables = NodePath("Vegetables");
//	vegetables.reparent_to(render);
//	for(int i = 0; i < qtd_vegetables; i++){
//		int chosen = rand() % vegetables_models.size();
//
//		NodePath vegetable = NodePath("vegetable instance");
//		vegetables_models[chosen].instance_to(vegetable);
//		vegetable.reparent_to(vegetables);
//
//		vegetable.set_h(rand() % 360);
//		vegetable.set_x(rand() % 250 * 4);
//		vegetable.set_y(rand() % 250 * 4);
//	}
//
//	PT(ImpostorPage) page1 = new ImpostorPage(qtd_vegetables, render, manager);
//	page1->generate(vegetables.get_children());


	/* Creates a PagedGeometry instance */
	PagedGeometry* trees = new PagedGeometry(camera, render, 64.0f);
	trees->setBounds(TBounds(-512,-512, 512, 512));


	trees->addDetailLevel<BatchPage>(150, 0, NULL, 0);

//	PT(MyHWInstancePage::MyHWInstancePageData) instance_data = new MyHWInstancePage::MyHWInstancePageData(instance_manager, 4);
	trees->addDetailLevel<MyHWInstancePage>(400, 0, NULL, 0);

	PT(MyTreePage::MyTreePageData) impostor_data = new MyTreePage::MyTreePageData(manager, 4);
	trees->addDetailLevel<MyTreePage>(650, 0, (void*)impostor_data, 0);
//	trees->addDetailLevel<BatchPage>(400, 0, NULL, 1);
//	trees->preloadGeometry(TBounds(-256,-256, 256, 256));

	MyTreeLoader* my_tree_loader = new MyTreeLoader();
	my_tree_loader->addTree(vegetables_models[0]);
	trees->setPageLoader(my_tree_loader);



//	/* The tree loader */
//	NodePath tree_example = window->load_model(render, "models/box");
//	TreeLoader3D* tree_loader = new TreeLoader3D(trees, TBounds(0, 0, 200, 200));
//	tree_loader->addTree(tree_example);
//	/* Assign the tree loader to the PagedGeometry */
//	trees->setPageLoader(tree_loader);


	/* Main Loop */
	while(framework.do_frame(Thread::get_current_thread())){
		trees->update();
	}
}




int main(int argc, char* argv[]){
	load_prc_file("config.prc");

	/* Open the framework and a window */
	framework.open_framework(argc, argv);
	window = framework.open_window();

	/* Configuring the debug input */
	window->setup_trackball();
	window->enable_keyboard();
	framework.enable_default_keys();


	/* Get the root of scene graph (render) and the default camera */
	NodePath render = window->get_render();
	NodePath camera = window->get_camera_group().get_child(0);



//	animated_pandas_sample();
//	return 0;

	paged_geometry_sample();
	return 0;



	return 0;
}

