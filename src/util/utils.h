/*
 * utils.h
 * Created on: 24/08/2012
 * Author: heekinho
 */

#ifndef UTILS_H
#define UTILS_H


#if defined(WIN32_VC) && defined(FORCE_INLINING)
// If FORCE_INLINING is defined, we use the keyword __forceinline,
// which tells MS VC++ to override its internal benefit heuristic
// and inline the fn if it is technically possible to do so.
#define INLINE __forceinline
#else
#define INLINE inline
#endif


/*! Obtém um número aleatório de min a max */
double random(double min, double max);


#endif /* UTILS_H */
