#ifndef BILLBOARDMAKER_H
#define BILLBOARDMAKER_H

#include "luse.h"
#include "nodePath.h"

class GraphicsWindow;
class GraphicsOutput;

/*! Armazena informações do card a ser criado */
struct CardInfo {
	LPoint3 position;
	LPoint2 size;
	LPoint3 center;
};

class BillboardMaker {
public:
	static NodePath make(PT(GraphicsWindow) gwin, const NodePath &object, LPoint2 size);

private:
	static CardInfo _get_card_info(const NodePath &object);
	static NodePath _make_camera(PT(GraphicsOutput) win);
	static void _save_buffer(PT(GraphicsOutput) buffer);
};

#endif
