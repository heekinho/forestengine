#include "billboardMaker.h"

#include "lens.h"
#include "perspectiveLens.h"
#include "orthographicLens.h"

#include "antialiasAttrib.h"
#include "pnmImage.h"

#include "graphicsOutput.h"
#include "graphicsEngine.h"
#include "camera.h"

NodePath BillboardMaker::make(PT(GraphicsWindow) gwin,
		const NodePath &object,
		LPoint2 size){
	/* We now get buffer thats going to hold the texture of our new scene */
	PT(GraphicsOutput) buffer = gwin->make_texture_buffer("spritebuf", size[0], size[1]/*, NULL, true*/);
	buffer->set_clear_color(LColor(0));

	/* Now we have to setup a new scene graph to make this scene */
	NodePath scene = NodePath("Scene");
	scene.set_antialias(AntialiasAttrib::M_auto);

	/* This takes care of setting up ther camera properly */
	NodePath camera = _make_camera(buffer);
	camera.reparent_to(scene);

	/* Get the teapot and rotates it for a simple animation */
	NodePath subject = NodePath("Subject");
	object.instance_to(subject);

	CardInfo info = _get_card_info(object);

	/* Lens configuration */
	PT(OrthographicLens) lens = new OrthographicLens();
	lens->set_film_size(info.size);
	camera.set_pos(info.position);
	DCAST(Camera, camera.node())->set_lens(lens);

	/* Coloca o objeto para ser visualizado */
	subject.reparent_to(scene);

	/* Renderiza a cena do buffer */
	buffer->get_engine()->render_frame();

	/* Faz o billboard, obtendo um texture card do buffer */
	NodePath card = buffer->get_texture_card();
	card.set_sx(info.size[0] / 2);
	card.set_sz(info.size[1] / 2);

	/* Configura o NodePath do Billboard centralizando corretamente */
	NodePath billboard = NodePath("Billboard");
	card.reparent_to(billboard);
	card.set_pos(info.center);

	/* Configura transparência do Billboard */
	billboard.set_transparency(TransparencyAttrib::M_dual);

	/* Remove o buffer liberando a memória e processamento */
	//_save_buffer(buffer);
	buffer->get_engine()->remove_window(buffer);

	return billboard;
}

CardInfo BillboardMaker::_get_card_info(const NodePath &object){
	CardInfo info;

	/* Calcula o menor bounds do objeto */
	LPoint3 min, max;
	object.calc_tight_bounds(min, max);

	/* Calcula o centro do card */
	info.center = (min + max) / 2;

	/* Calcula a posição do card */
	info.position = LPoint3(info.center);
	info.position.set_y(min.get_y() - 1);

	/* Calcula o tamanho relativo do card */
	info.size = LPoint2(fabs(min.get_x()) + fabs(max.get_x()), fabs(min.get_z()) + fabs(max.get_z()));

	return info;
}

/*! Cria e configura a camera */
NodePath BillboardMaker::_make_camera(PT(GraphicsOutput) win){
    PT(DisplayRegion) dr = win->make_display_region();
	PT(Camera) camera = new Camera("cam");

	/* Configuração do Display Region */
	NodePath camera_np = NodePath(camera);
    dr->set_camera(camera_np);

    return camera_np;
}

/*! Tira um screenshot do buffer */
void BillboardMaker::_save_buffer(PT(GraphicsOutput) buffer){
	PNMImage screenshot;
	buffer->get_texture()->store(screenshot);
	screenshot.write("shadow-buffer.png");
}
