/*
 * utils.cxx
 * Created on: 24/08/2012
 * Author: heekinho
 */

#include "utils.h"

#include "stdlib.h"

/*! Obtém um número aleatório de min a max */
double random(double min, double max){
	double range = max - min;
	return (double(rand())/double(RAND_MAX)) * range + min;
}
