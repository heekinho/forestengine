#ifndef PAGEDGEOMETRY_H
#define PAGEDGEOMETRY_H

#include "pandaFramework.h"

#include "tbounds.h"
#include "pageLoader.h"

class GeometryPageManager;

class PagedGeometry {
public:
	PagedGeometry(const NodePath &camera = NodePath(), const NodePath &scene = NodePath(), float pageSize = 100);
	~PagedGeometry();

	template <class PageType> INLINE GeometryPageManager* addDetailLevel(float maxRange, float transitionLength = 0,
			void* data = NULL, int queryFlag = 0);
	INLINE const std::list<GeometryPageManager*> &getDetailLevels() { return managerList; }

	void setPageSize(float size);
	INLINE float getPageSize() const { return pageSize; }

	void setBounds(const TBounds bounds);
	void setInfinite();
	INLINE const TBounds &getBounds() const { return m_bounds; }

	void setPageLoader(PageLoader *loader);
	INLINE PageLoader *getPageLoader(){ return pageLoader; }


	void removeDetailLevels();

	/** Sets the output directory for the imposter pages */
	void setTempDir(const string &dir);
	const string &getTempdir() { return this->tempdir; };



	void setCamera(const NodePath &cam);
	INLINE NodePath getCamera() const { return sceneCam; }

	INLINE NodePath getSceneNode() const { return rootNode; }

//	/**
//	\brief Convert an Ogre::AxisAlignedBox to a TBounds coplanar to the plane defined by the UP axis.
//	*/
//	TBounds convertAABToTBounds( const Ogre::AxisAlignedBox & aab ) const;

	void update();

	void reloadGeometry();
	void reloadGeometryPage(const LPoint3f &point);
	void reloadGeometryPages(const LPoint3f &center, float radius);
	void reloadGeometryPages(const TBounds &area);
	void preloadGeometry(const TBounds & area);
	void resetPreloadedGeometry();

	void setVisible(bool visible) { geometryAllowedVisible = visible; }
	bool getVisible() { return geometryAllowedVisible; }

	void setShadersEnabled(bool value) { shadersEnabled = value; }
	bool getShadersEnabled() { return shadersEnabled; }

	void setCustomParam( std::string entity, std::string paramName, float paramValue);
	void setCustomParam( std::string paramName, float paramValue);
	float getCustomParam( std::string entity, std::string paramName, float defaultParamValue) const;
	float getCustomParam( std::string paramName, float defaultParamValue) const;

protected:
	void _addDetailLevel(GeometryPageManager *mgr, float maxRange, float transitionLength);

	//This list keeps track of all the GeometryPageManager's added with addPageManager()
	list<GeometryPageManager*> managerList;

	//The bounds and page size
	TBounds m_bounds;
	float pageSize;

	NodePath rootNode;				//PagedGeometry's own "root" node
	bool shadersEnabled;

	bool geometryAllowedVisible;	//If set to false, all geometry managed by this PagedGeometry is hidden

	//Camera data
	NodePath sceneCam;
	LPoint3f oldCamPos;

	NodePath lastSceneCam;
	LPoint3f lastOldCamPos;

	PT(ClockObject) timer;
	double lastTime;

	//The assigned PageLoader used to load entities
	PageLoader *pageLoader;

	string tempdir;

private:
	map<string, float> customParam;
};

#include "geometryPageManager.h"

template <class PageType> INLINE GeometryPageManager* PagedGeometry::addDetailLevel(float maxRange, float transitionLength, void* data, int queryFlag){
	// Create a new page manager
	GeometryPageManager *mgr = new GeometryPageManager(this);

	// Add it to the list (also initializing maximum viewing distance)
	_addDetailLevel(mgr, maxRange, transitionLength);

	//And initialize the paged (dependent on maximum viewing distance)
	mgr->initPages<PageType>(getBounds(), data, queryFlag);

	return mgr;
}

#endif
