#ifndef TBOUNDS_H
#define TBOUNDS_H

#include <math.h>

class TBounds {
public:
	TBounds(float left = 0, float top = 0, float right = 0, float bottom = 0){
		TBounds::left = left;
		TBounds::top = top;
		TBounds::right = right;
		TBounds::bottom = bottom;
	}

	float /*inline*/ width() const { return fabs(left - right); };
	float /*inline*/ height() const { return fabs(top - bottom); };

	float top, left, right, bottom;
};

#endif
