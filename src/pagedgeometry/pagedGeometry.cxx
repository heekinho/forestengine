#include "pagedGeometry.h"

PagedGeometry::PagedGeometry(const NodePath &cam, const NodePath &scene, float pageSize){
	if(!cam.is_empty()){
		sceneCam = cam;
		rootNode = scene;
		oldCamPos = cam.get_pos(rootNode);
	}
	else {
		sceneCam = NodePath();
		rootNode = NodePath();
		oldCamPos = LPoint3f::zero();
	}

	lastSceneCam = NodePath();
	lastOldCamPos = LPoint3f::zero();

	// Init. timer TODO: Usar global clock?
	timer = ClockObject::get_global_clock();
	lastTime = timer->get_long_time();

	this->pageSize = pageSize;
	this->m_bounds = TBounds(0, 0, 0, 0);

	pageLoader = NULL;
	geometryAllowedVisible = true;
	tempdir=""; // empty for current working directory
	shadersEnabled = true;
}

PagedGeometry::~PagedGeometry(){
	removeDetailLevels();
}


void PagedGeometry::removeDetailLevels(){
	list<GeometryPageManager*>::iterator it;

	//Delete all the page managers
	for (it = managerList.begin(); it != managerList.end(); ++it){
		GeometryPageManager *mgr = *it;
		delete mgr;
	}

	//Clear the page manager list
	managerList.clear();
}


void PagedGeometry::_addDetailLevel(GeometryPageManager *mgr, float maxRange, float transitionLength){
	/* Calcula o range "near" */
	float minRange = 0;

	/* Verifica se já existe manager e pega a última farDistance para ser o near*/
	if(!managerList.empty()){
		GeometryPageManager *lastMgr = managerList.back();
		minRange = lastMgr->getFarRange();
	}

	// Error check
	if (maxRange <= minRange) nout << "Closer detail levels must be added before farther ones" << endl;

	// Setup the new manager
	mgr->setNearRange(minRange);
	mgr->setFarRange(maxRange);
	mgr->setTransition(transitionLength);

	managerList.push_back(mgr);
}



void PagedGeometry::setTempDir(const string &dir){
   tempdir = dir;
}

void PagedGeometry::setPageLoader(PageLoader *loader){
	pageLoader = loader;
}

void PagedGeometry::setCamera(const NodePath &cam){
	if (cam == lastSceneCam){
		//If the cache values for this camera are preserved, use them
		std::swap(oldCamPos, lastOldCamPos);
		std::swap(sceneCam, lastSceneCam);
	} else {
		lastSceneCam = sceneCam;
		lastOldCamPos = oldCamPos;
		sceneCam = cam;
	}
}

////Default coordinate system - no conversion
//Vector3 PagedGeometry::_convertToLocal(const Vector3 &globalVec) const{
//	return globalVec;
//}
//
void PagedGeometry::setPageSize(float size){
	if (!managerList.empty())
		nout << "PagedGeometry::setPageSize() cannot be called after detail levels have been added. Call removeDetailLevels() first." << endl;

	pageSize = size;
}

void PagedGeometry::setInfinite(){
	if (!managerList.empty())
		nout << "PagedGeometry::setInfinite() cannot be called after detail levels have been added. Call removeDetailLevels() first." << endl;

	m_bounds = TBounds(0, 0, 0, 0);
}

void PagedGeometry::setBounds(TBounds bounds){
	if (!managerList.empty())
		nout << "PagedGeometry::setBounds() cannot be called after detail levels have been added. Call removeDetailLevels() first." << endl;
	if (bounds.width() <= 0 || bounds.height() <=0)
		nout << "Bounds must have positive width and height, PagedGeometry::setBounds()" << endl;
	if (fabs(bounds.width() - bounds.height()) > 0.01)
		nout << "Bounds must be square, PagedGeometry::setBounds()" << endl;

	m_bounds = bounds;
}

//TBounds PagedGeometry::convertAABToTBounds( const Ogre::AxisAlignedBox & aab ) const
//{
//	Vector3 minimum = _convertToLocal(aab.getMinimum());
//	Vector3 maximum = _convertToLocal(aab.getMaximum());
//	return TBounds (minimum.x, minimum.z, maximum.x, maximum.z);
//}


void PagedGeometry::update(){
	//If no camera has been set, then return without doing anything
	if (sceneCam.is_empty()) return;

	//Calculate time since last update
	double deltaTime, tmp;
	tmp = timer->get_long_time();
	deltaTime = tmp - lastTime;
	lastTime = tmp;

	deltaTime *= 1000; // Foi implementado em Milisegundos.

	//Get camera position and speed
	//Vector3 camPos = _convertToLocal(sceneCam->getDerivedPosition());
	LPoint3f camPos = sceneCam.get_pos(rootNode);

	LVector3f camSpeed;	//Speed in units-per-millisecond
	if (deltaTime == 0) camSpeed = LVector3f(0);
	else camSpeed = (camPos - oldCamPos) / deltaTime;

	oldCamPos = camPos;

	if (pageLoader){
		//Update the PageLoader
		pageLoader->frameUpdate();

		//Update all the page managers
		bool enableCache = true;
		std::list<GeometryPageManager *>::iterator it;
		GeometryPageManager *prevMgr = NULL;
		for (it = managerList.begin(); it != managerList.end(); ++it){
			GeometryPageManager *mgr = *it;
			mgr->update(deltaTime, camPos, camSpeed, enableCache, prevMgr);
			prevMgr = mgr;
		}
	}

//	//Update misc. subsystems
//	StaticBillboardSet::updateAll(_convertToLocal(getCamera()->getDerivedDirection()));
}

void PagedGeometry::reloadGeometry(){
	//assert(pageLoader);
	if(!pageLoader) nout << "pageLoader não definido." << endl;

	list<GeometryPageManager *>::iterator it;
	for (it = managerList.begin(); it != managerList.end(); ++it){
		GeometryPageManager *mgr = *it;
		mgr->reloadGeometry();
	}
}

void PagedGeometry::reloadGeometryPage(const LPoint3f &point){
	if (!pageLoader) return;

	std::list<GeometryPageManager *>::iterator it;
	for (it = managerList.begin(); it != managerList.end(); ++it){
		GeometryPageManager *mgr = *it;
		mgr->reloadGeometryPage(point);
	}
}

void PagedGeometry::reloadGeometryPages(const LPoint3f &center, float radius){
	if (!pageLoader) return;

	std::list<GeometryPageManager *>::iterator it;
	for (it = managerList.begin(); it != managerList.end(); ++it){
		GeometryPageManager *mgr = *it;
		mgr->reloadGeometryPages(center, radius);
	}
}

void PagedGeometry::reloadGeometryPages(const TBounds &area){
	if (!pageLoader) return;

	TBounds localArea = area;

	if (localArea.left > localArea.right) std::swap(localArea.left, localArea.right);
	if (localArea.top > localArea.bottom) std::swap(localArea.top, localArea.bottom);

	std::list<GeometryPageManager *>::iterator it;
	for (it = managerList.begin(); it != managerList.end(); ++it){
		GeometryPageManager *mgr = *it;
		mgr->reloadGeometryPages(localArea);
	}
}

void PagedGeometry::preloadGeometry(const TBounds &area){
	if (!pageLoader) return;

	TBounds localArea = area;

	if (localArea.left > localArea.right) std::swap(localArea.left, localArea.right);
	if (localArea.top > localArea.bottom) std::swap(localArea.top, localArea.bottom);

	std::list<GeometryPageManager *>::iterator it;
	for (it = managerList.begin(); it != managerList.end(); ++it){
		GeometryPageManager *mgr = *it;
		mgr->preloadGeometry(localArea);
	}
}

void PagedGeometry::resetPreloadedGeometry(){
	if (!pageLoader) return;

	std::list<GeometryPageManager *>::iterator it;
	for (it = managerList.begin(); it != managerList.end(); ++it){
		GeometryPageManager *mgr = *it;
		mgr->resetPreloadedGeometry();
	}
}


void  PagedGeometry::setCustomParam(string entity, string paramName, float paramValue){
	setCustomParam(entity + "." + paramName, paramValue);
}

void  PagedGeometry::setCustomParam(string paramName, float paramValue){
	customParam[paramName] = paramValue;
}

float PagedGeometry::getCustomParam(string entity, string paramName, float defaultParamValue) const {
	return getCustomParam(entity + "." + paramName, defaultParamValue);
}

float PagedGeometry::getCustomParam(string paramName, float defaultParamValue) const {
	std::map<string, float>::const_iterator it;
	it = customParam.find(paramName);
	if (it != customParam.end()) {
		float x = it->second;
		return x;
	}
	else return defaultParamValue;
}
