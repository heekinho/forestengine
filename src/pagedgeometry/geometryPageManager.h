#ifndef GEOMETRYPAGEMANAGER_H
#define GEOMETRYPAGEMANAGER_H

#include "pandaFramework.h"

#include "tbounds.h"
#include "geometryPage.h"

class PagedGeometry;

class GeometryPageManager {
public:
	typedef list<GeometryPage*> TPGeometryPages;

	GeometryPageManager(PagedGeometry *main_geom);
	~GeometryPageManager();

	INLINE void setNearRange(float nearRange){
		nearDist = nearRange;
		nearDistSq = nearDist * nearDist;
	}

	INLINE void setFarRange(float farRange){
		farDist = farRange;
		farDistSq = farDist * farDist;

		farTransDist = farDist + fadeLength;
		farTransDistSq = farTransDist * farTransDist;
	}

	INLINE float getNearRange() const {
		return nearDist;
	}

	INLINE float getFarRange() const {
		return farDist;
	}

	INLINE void setTransition(float transitionLength){
		if (transitionLength > 0) {
			// Setup valid transition
			fadeLength = transitionLength;
			fadeLengthSq = fadeLength * fadeLength;
			fadeEnabled = true;
		} else {
			// <= 0 indicates disabled transition
			fadeLength = 0;
			fadeLengthSq = 0;
			fadeEnabled = false;
		}

		farTransDist = farDist + fadeLength;
		farTransDistSq = farTransDist * farTransDist;
	}

	INLINE float getTransition() const {
		return fadeLength;
	}

	INLINE void setCacheSpeed(unsigned long maxCacheInterval = 200, unsigned long inactivePageLife = 2000){
		GeometryPageManager::maxCacheInterval = maxCacheInterval;
		GeometryPageManager::inactivePageLife = inactivePageLife;
	}

	/** \brief Internal functions - DO NOT USE */
	INLINE TPGeometryPages getLoadedPages() const { return loadedList; }
	template <class PageType> void initPages(const TBounds& bounds, void* data, int queryFlag = 0);
	void update(double deltaTime, LPoint3f &camPos, LVector3f &camSpeed, bool &enableCache, GeometryPageManager *prevManager);
	//TODO: IMPLEMENTAR NO CXX. COLOQUEI SÓ PRA TESTAR
	void reloadGeometry();
	void reloadGeometryPage(const LPoint3f &point);
	void reloadGeometryPages(const LPoint3f &center, float radius);
	void reloadGeometryPages(const TBounds & area);
	void preloadGeometry(const TBounds & area);
	void resetPreloadedGeometry();
private:
	PagedGeometry *mainGeom;

	// geomGrid is a 2D array storing all the GeometryPage instances managed by this object.
	GeometryPage **geomGrid;		// A dynamic 2D array of pointers (2D grid of GeometryPage's)
	GeometryPage **scrollBuffer;	// A dynamic 1D array of pointers (temporary GeometryPage's used in scrolling geomGrid)
	int geomGridX, geomGridZ;		// The dimensions of the dynamic array
	TBounds	gridBounds;				// Current grid bounds

	//INLINE function used to get geometry page tiles
	INLINE GeometryPage *_getGridPage(const int x, const int z){
		return geomGrid[z * geomGridX + x];
	}
	INLINE void _setGridPage(const int x, const int z, GeometryPage *page){
		geomGrid[z * geomGridX + x] = page;
	}

	//Utility functions for loading/unloading geometry pages (see source for detailed descriptions)
	void _loadPage(GeometryPage *page);
	void _unloadPage(GeometryPage *page);
	void _unloadPageDelayed(GeometryPage *page);

	// Utility function for scrolling pages in the grid by the given amount
	void _scrollGridPages(int shiftX, int shiftZ);


	// Timer counting how long it has been since the last page has been cached
	unsigned long cacheTimer;

	TPGeometryPages pendingList;	// Pages of geometry to be loaded
	TPGeometryPages loadedList;		// Pages of geometry already loaded

	// Cache settings
	unsigned long maxCacheInterval;
	unsigned long inactivePageLife;

	// Near and far visibility ranges for this type of geometry
	float nearDist, nearDistSq;
	float farDist, farDistSq;
	float farTransDist, farTransDistSq;	//farTransDist = farDist + fadeLength

	//Fade transitions
	float fadeLength, fadeLengthSq;
	bool fadeEnabled;
};

#include "pagedGeometry.h"

template <class PageType> INLINE void GeometryPageManager::initPages(const TBounds& bounds, void* data, int queryFlag){
	// Calculate grid size, if left is Real minimum, it means that bounds are infinite
	// scrollBuffer is used as a flag. If it is allocated than infinite bounds are used
	// !!! Two cases are required because of the way scrolling is implemented
	// if it is redesigned it would allow to use the same functionality.
	if(bounds.width() < 0.00001){
		// In case of infinite bounds bounding rect needs to be calculated in a different manner, since
		// it represents local bounds, which are shifted along with the player's movements around the world.
		geomGridX = (2 * farTransDist / mainGeom->getPageSize()) + 4;
		gridBounds.top = 0;
		gridBounds.left = 0;
		gridBounds.right = geomGridX * mainGeom->getPageSize();
		gridBounds.bottom = geomGridX * mainGeom->getPageSize();
		// Allocate scroll buffer (used in scrolling the grid)
		scrollBuffer = new GeometryPage *[geomGridX];

		//Note: All this padding and transition preparation is performed because even in infinite
		//mode, a local grid size must be chosen. Unfortunately, this also means that view ranges
		//and transition lengths cannot be exceeded dynamically with set functions.
	}
	else {
		//Bounded mode
		gridBounds = bounds;
		// In case the devision does not give the round number use the next largest integer
		geomGridX = ceil(gridBounds.width() / mainGeom->getPageSize());
	}
	geomGridZ = geomGridX; //Note: geomGridX == geomGridZ; Need to merge.


	//Allocate grid array
	geomGrid = new GeometryPage *[geomGridX * geomGridZ];

	int xioffset = floor(gridBounds.left / mainGeom->getPageSize());
	int zioffset = floor(gridBounds.top / mainGeom->getPageSize());
	for (int x = 0; x < geomGridX; ++x){
		for (int z = 0; z < geomGridZ; ++z){
			GeometryPage* page = new PageType();
			page->init(mainGeom, data);
			// 0,0 page is located at (gridBounds.left,gridBounds.top) corner of the bounds

			// TODO: Depois tem ki fazer um processo de conversão de z para y.
			page->_centerPoint.set_x( ((x + 0.5f) * mainGeom->getPageSize()) + gridBounds.left );
			page->_centerPoint.set_y( ((z + 0.5f) * mainGeom->getPageSize()) + gridBounds.top  );
			page->_centerPoint.set_z(0.0f);

			page->_xIndex = x + xioffset;
			page->_zIndex = z + zioffset;
			page->_inactiveTime = 0;
			page->_loaded = false;
			page->_needsUnload = false;
			page->_pending = false;
			page->_keepLoaded = false;
			page->_visible = false;
			page->_userData = 0;
			page->_fadeEnable = false;
			page->setQueryFlag(queryFlag);

			page->clearBoundingBox();

			_setGridPage(x, z, page);
		}
	}
}

#endif
