#ifndef GEOMETRYPAGE_H
#define GEOMETRYPAGE_H

#include "pandaFramework.h"
#include "boundingBox.h"

class PagedGeometry;

class GeometryPage {
	friend class GeometryPageManager;
public:
	GeometryPage();
	virtual ~GeometryPage() {}

	virtual void init(PagedGeometry *geom, void* data) = 0;

	void setQueryFlag(int flag){
		mHasQueryFlag = true;
		mQueryFlag = flag;
	};

	bool hasQueryFlag()	{ return mHasQueryFlag;	};
	int getQueryFlag(){	return mQueryFlag; };

	virtual void addEntity(NodePath &entity) = 0;
	virtual void addEntityToBoundingBox(const NodePath &entity);

	virtual void removeEntities() = 0;

	virtual CPT(BoundingBox) getBoundingBox();
	virtual void clearBoundingBox();

	INLINE LPoint3f &getCenterPoint() { return _centerPoint; }
	virtual void setRegion(float left, float top, float right, float bottom) {};
	virtual void setFade(bool enabled, float visibleDist = 0, float invisibleDist = 0) = 0;
	virtual void setVisible(bool visible) = 0;
	INLINE bool isVisible() { return (_visible && _loaded); }

	virtual void build() {}
	virtual void update() {}
private:
	//These values and functions are used by the GeometryPageManager internally.
	LPoint3f _centerPoint;		//The central point of this page (used to visibility calculation)
	int _xIndex, _zIndex;			//The absolute grid position of this page
	unsigned long _inactiveTime;	//How long this page has been inactive (used to calculate expired pages)
	bool _visible;		//Flag indicating if page is visible
	bool _fadeEnable;	//Flag indicating if page fading is enabled

	bool _pending;		//Flag indicating if page needs loading
	bool _loaded;		//Flag indicating if page is loaded
	bool _needsUnload;	//Flag indicating if page needs unloading before next load
	bool _keepLoaded;	//Flag indicating if the page should not be unloaded
	list<GeometryPage*>::iterator _iter;	//Iterator in loadedList

	PT(BoundingBox) _trueBounds;		//Actual bounding box of the 3D geometry added to this page
	bool _trueBoundsUndefined;			//Flag indicating if _trueBounds has not been defined yet

	void *_userData;	//Misc. data associated with this page by the PageLoader

	bool mHasQueryFlag;
	int mQueryFlag;
};

#endif
