#include "geometryPageManager.h"

GeometryPageManager::GeometryPageManager(PagedGeometry *mainGeom)
: mainGeom(mainGeom)
, cacheTimer(0) // Reset the cache timer
, scrollBuffer(NULL)
, geomGrid(NULL)
, geomGridX(0)
, geomGridZ(0)
{

	// Use default cache speeds
	setCacheSpeed();

	// No transition default
	setTransition(0);
}

GeometryPageManager::~GeometryPageManager(){
	//Delete GeometryPage's
	for (int x = 0; x < geomGridX; ++x)
		for (int z = 0; z < geomGridZ; ++z)
			delete _getGridPage(x, z);

	//Deallocate arrays
	if(geomGrid) delete[] geomGrid;
	if(scrollBuffer) delete[] scrollBuffer;
}

void GeometryPageManager::update(double deltaTime, LPoint3f &camPos, LVector3f &camSpeed, bool &enableCache, GeometryPageManager *prevManager){
	//-- Cache new geometry pages --
	//nout << loadedList.size() << endl;

	//Cache 1 page ahead of the view ranges
	const float cacheDist = farTransDist + mainGeom->getPageSize();
	const float cacheDistSq = cacheDist * cacheDist;

	//First calculate the general area where the pages will be processed
	// 0,0 is the left top corner of the bounding box
	int x1 = floor(((camPos.get_x() - cacheDist) - gridBounds.left) / mainGeom->getPageSize());
	int x2 = floor(((camPos.get_x() + cacheDist) - gridBounds.left) / mainGeom->getPageSize());
	int z1 = floor(((camPos.get_y() - cacheDist) - gridBounds.top) / mainGeom->getPageSize());
	int z2 = floor(((camPos.get_y() + cacheDist) - gridBounds.top) / mainGeom->getPageSize());

	if(scrollBuffer){
		//Check if the page grid needs to be scrolled
		int shiftX = 0, shiftZ = 0;
		if (x1 < 0) shiftX = x1; else if (x2 >= geomGridX-1) shiftX = x2 - (geomGridX-1);
		if (z1 < 0) shiftZ = z1; else if (z2 >= geomGridZ-1) shiftZ = z2 - (geomGridZ-1);
		if (shiftX != 0 || shiftZ != 0){
			//Scroll grid
			_scrollGridPages(shiftX, shiftZ);

			//Update grid bounds and processing area
			gridBounds.left += shiftX * mainGeom->getPageSize();
			gridBounds.right += shiftX * mainGeom->getPageSize();
			gridBounds.top += shiftZ * mainGeom->getPageSize();
			gridBounds.bottom += shiftZ * mainGeom->getPageSize();
			x1 -= shiftX;  x2 -= shiftX;
			z1 -= shiftZ;  z2 -= shiftZ;
		}
	}
	else {
		// make sure that values are inbounds
		if(x2 >= geomGridX) x2 = geomGridX - 1;
		if(z2 >= geomGridZ) z2 = geomGridZ - 1;

		if (x1 < 0) x1 = 0;
		if (z1 < 0) z1 = 0;
	}

	//Now, in that general area, find what pages are within the cacheDist radius
	//Pages within the cacheDist radius will be added to the pending block list
	//to be loaded later, and pages within farDist will be loaded immediately.
	for (int x = x1; x <= x2; ++x){
		for (int z = z1; z <= z2; ++z){
			GeometryPage *blk = _getGridPage(x, z);

			float dx = camPos.get_x() - blk->_centerPoint.get_x();
			float dz = camPos.get_y() - blk->_centerPoint.get_y();
			float distSq = dx * dx + dz * dz;

			//If the page is in the cache radius...
			if (distSq <= cacheDistSq){
				//If the block hasn't been loaded yet, it should be
				if (blk->_loaded == false){
					//Test if the block's distance is between nearDist and farDist
					if (distSq >= nearDistSq && distSq < farTransDistSq){
						//If so, load the geometry immediately
						_loadPage(blk);
						loadedList.push_back(blk);
						blk->_iter = (--loadedList.end());

						//And remove it from the pending list if necessary
						if (blk->_pending){
							pendingList.remove(blk);
							blk->_pending = false;
						}
					} else {
						//Otherwise, add it to the pending geometry list (if not already)
						//Pages in then pending list will be loaded later (see below)
						if (!blk->_pending){
							pendingList.push_back(blk);
							blk->_pending = true;
						}
					}
				} else {
					//Set the inactive time to 0 (since the page is active). This
					//must be done in order to keep it from expiring (and deleted).
					//This way, blocks not in the cache radius won't have their
					//inactivity clock reset, and they will expire in a few seconds.
					blk->_inactiveTime = 0;
				}
			}
		}
	}


	//Calculate cache speeds based on camera speed. This is important to keep the cache
	//process running smooth, because if the cache can't keep up with the camera, the
	//immediately visible pages will be forced to load instantly, which can cause
	//noticeable and sudden stuttering. The cache system results in smoother performance
	//because it smooths the loading tasks out across multiple frames. For example,
	//instead of loading 10+ blocks every 2 seconds, the cache would load 1 block every
	//200 milliseconds.
	float speed = sqrt(camSpeed.get_x() * camSpeed.get_x() + camSpeed.get_y() * camSpeed.get_y());

	unsigned long cacheInterval;
	if (speed == 0) cacheInterval = maxCacheInterval;
	else {
		cacheInterval = (mainGeom->getPageSize() * 0.8f) / (speed * pendingList.size());
		if (cacheInterval > maxCacheInterval) cacheInterval = maxCacheInterval;
	}


	TPGeometryPages::iterator i1, i2;

	//Now load a single geometry page periodically, based on the cacheInterval
	cacheTimer += deltaTime;
	if (cacheTimer >= cacheInterval && enableCache){
		//Find a block to be loaded from the pending list
		i1 = pendingList.begin();
		i2 = pendingList.end();
		while (i1 != i2){
			GeometryPage *blk = *i1;

			//Remove it from the pending list
			i1 = pendingList.erase(i1);
			blk->_pending = false;

			//If it's within the geometry cache radius, load it and break out of the loop
			float dx = camPos.get_x() - blk->_centerPoint.get_x();
			float dz = camPos.get_y() - blk->_centerPoint.get_y();
			float distSq = dx * dx + dz * dz;
			if (distSq <= cacheDistSq){
				_loadPage(blk);
				loadedList.push_back(blk);
				blk->_iter = (--loadedList.end());

				enableCache = false;
				break;
			}

			//Otherwise this will keep looping until an unloaded page is found
		}

		//Reset the cache timer
		cacheTimer = 0;
	}


	//-- Update existing geometry and impostors --

	//Loop through each loaded geometry block
	i1 = loadedList.begin();
	i2 = loadedList.end();

	//Real fadeBeginDistSq = farDistSq - Math::Sqr(mainGeom->getPageSize() * 1.4142f);
	float halfPageSize = mainGeom->getPageSize() * 0.5f;
	while (i1 != i2){
		GeometryPage *blk = *i1;

		//If the geometry has expired...
		if (blk->_inactiveTime >= inactivePageLife){
			if (!blk->_keepLoaded) {
				//Unload it
				_unloadPage(blk);
				i1 = loadedList.erase(i1);
			}
			else {
				//This page needs to be kept loaded indefinitely, so don't unload it
				blk->_inactiveTime = 0;
				++i1;
			}
		}
		else {
			//Update it's visibility/fade status based on it's distance from the camera
			bool visible = false;
			float dx = camPos.get_x() - blk->_centerPoint.get_x();
			float dz = camPos.get_y() - blk->_centerPoint.get_y();
			float distSq = dx * dx + dz * dz;

			float overlap = 0, tmp;

			tmp = blk->_trueBounds->get_max().get_x() - halfPageSize;
			if (tmp > overlap) overlap = tmp;
			tmp = blk->_trueBounds->get_max().get_y() - halfPageSize;
			if (tmp > overlap) overlap = tmp;
			tmp = blk->_trueBounds->get_min().get_x() + halfPageSize;
			if (tmp > overlap) overlap = tmp;
			tmp = blk->_trueBounds->get_min().get_y() + halfPageSize;
			if (tmp > overlap) overlap = tmp;

			float pageLengthSq = pow((mainGeom->getPageSize() + overlap) * 1.41421356f, 2);

			if (distSq + pageLengthSq >= nearDistSq && distSq - pageLengthSq < farTransDistSq){
				//Fade the page when transitioning
				bool enable = false;
				float fadeNear = 0;
				float fadeFar = 0;

				if (fadeEnabled && distSq + pageLengthSq >= farDistSq){
					//Fade in
					visible = true;
					enable = true;
					fadeNear = farDist;
					fadeFar = farTransDist;
				}
				else if (prevManager && prevManager->fadeEnabled && (distSq - pageLengthSq < prevManager->farTransDistSq)){
					//Fade out
					visible = true;
					enable = true;
					fadeNear = prevManager->farDist + (prevManager->farTransDist - prevManager->farDist) * 0.5f;	//This causes geometry to fade out faster than it fades in, avoiding a state where a transition appears semitransparent
					fadeFar = prevManager->farDist;
				}

				//Apply fade settings
				if (enable != blk->_fadeEnable){
					blk->setFade(enable, fadeNear, fadeFar);
					blk->_fadeEnable = enable;
				}
			}
			//Non-fade visibility
			if (distSq >= nearDistSq && distSq < farDistSq) visible = true;
			//Hide all?
			if (!mainGeom->getVisible()) visible = false;

			//Update visibility
			if (visible){
				//Show the page if it isn't visible
				if (blk->_visible != true){
					blk->setVisible(true);
					blk->_visible = true;
				}
			} else {
				//Hide the page if it's not already
				if (blk->_visible != false){
					blk->setVisible(false);
					blk->_visible = false;
				}
			}

			//And update it
			blk->update();

			++i1;
		}

		//Increment the inactivity timer for the geometry
		blk->_inactiveTime += deltaTime;
	}
}

//Clears all GeometryPage's
void GeometryPageManager::reloadGeometry(){
	TPGeometryPages::iterator it;
	for (it = loadedList.begin(); it != loadedList.end(); ++it){
		GeometryPage *page = *it;
		_unloadPage(page);
	}
	loadedList.clear();
}

//Clears a single page (which contains the given point)
void GeometryPageManager::reloadGeometryPage(const LPoint3f &point){
	//Determine which grid block contains the given points
	const int x = floor(geomGridX * (point.get_x() - gridBounds.left) / gridBounds.width());
	const int z = floor(geomGridZ * (point.get_y() - gridBounds.top) / gridBounds.height());

	//Unload the grid block if it's in the grid area, and is loaded
	if (x >= 0 && z >= 0 && x < geomGridX && z < geomGridZ){
		GeometryPage *page = _getGridPage(x, z);
		if (page->_loaded){
			_unloadPage(page);
			loadedList.erase(page->_iter);
		}
	}
}

//Clears pages within the radius
void GeometryPageManager::reloadGeometryPages(const LPoint3f &center, float radius){
	//First calculate a square boundary to eliminate the search space
	TBounds area(center.get_x() - radius, center.get_y() - radius, center.get_x() + radius, center.get_y() + radius);

	//Determine which grid block contains the top-left corner
	int x1 = floor(geomGridX * (area.left - gridBounds.left) / gridBounds.width());
	int z1 = floor(geomGridZ * (area.top - gridBounds.top) / gridBounds.height());
	if (x1 < 0) x1 = 0; else if (x1 > geomGridX-1) x1 = geomGridX-1;
	if (z1 < 0) z1 = 0; else if (z1 > geomGridZ-1) z1 = geomGridZ-1;
	//...and the bottom right
	int x2 = floor(geomGridX * (area.right - gridBounds.left) / gridBounds.width());
	int z2 = floor(geomGridZ * (area.bottom - gridBounds.top) / gridBounds.height());
	if (x2 < 0) x2 = 0; else if (x2 > geomGridX-1) x2 = geomGridX-1;
	if (z2 < 0) z2 = 0; else if (z2 > geomGridZ-1) z2 = geomGridZ-1;

	//Scan all the grid blocks in the region
	float radiusSq = radius * radius;
	for (int x = x1; x <= x2; ++x) {
		for (int z = z1; z <= z2; ++z) {
			GeometryPage *page = _getGridPage(x, z);
			if (page->_loaded){
				LPoint3f pos = LPoint3f(page->getCenterPoint());
				float distX = (pos.get_x() - center.get_x()), distZ = (pos.get_y() - center.get_y());
				float distSq = distX * distX + distZ * distZ;

				if (distSq <= radius) {
					_unloadPage(page);
					loadedList.erase(page->_iter);
				}
			}
		}
	}
}

//Clears pages within the bounds
void GeometryPageManager::reloadGeometryPages(const TBounds & area){
	//Determine which grid block contains the top-left corner
	int x1 = floor(geomGridX * (area.left - gridBounds.left) / gridBounds.width());
	int z1 = floor(geomGridZ * (area.top - gridBounds.top) / gridBounds.height());
	if (x1 < 0) x1 = 0; else if (x1 > geomGridX-1) x1 = geomGridX-1;
	if (z1 < 0) z1 = 0; else if (z1 > geomGridZ-1) z1 = geomGridZ-1;
	//...and the bottom right
	int x2 = floor(geomGridX * (area.right - gridBounds.left) / gridBounds.width());
	int z2 = floor(geomGridZ * (area.bottom - gridBounds.top) / gridBounds.height());
	if (x2 < 0) x2 = 0; else if (x2 > geomGridX-1) x2 = geomGridX-1;
	if (z2 < 0) z2 = 0; else if (z2 > geomGridZ-1) z2 = geomGridZ-1;

	//Unload the grid blocks
	for (int x = x1; x <= x2; ++x) {
		for (int z = z1; z <= z2; ++z) {
			GeometryPage *page = _getGridPage(x, z);
			if (page->_loaded){
				_unloadPage(page);
				loadedList.erase(page->_iter);
			}
		}
	}
}

//Loads pages within viewing range of the bounds and keeps them loaded
void GeometryPageManager::preloadGeometry(const TBounds & area){
	TBounds loadarea;
	loadarea.left = area.left - farDist;
	loadarea.right = area.right + farDist;
	loadarea.top = area.top - farDist;
	loadarea.bottom = area.bottom + farDist;

	//Determine which grid block contains the top-left corner
	int x1 = floor(geomGridX * (loadarea.left - gridBounds.left) / gridBounds.width());
	int z1 = floor(geomGridZ * (loadarea.top - gridBounds.top) / gridBounds.height());
	if (x1 < 0) x1 = 0; else if (x1 > geomGridX-1) x1 = geomGridX-1;
	if (z1 < 0) z1 = 0; else if (z1 > geomGridZ-1) z1 = geomGridZ-1;
	//...and the bottom right
	int x2 = floor(geomGridX * (loadarea.right - gridBounds.left) / gridBounds.width());
	int z2 = floor(geomGridZ * (loadarea.bottom - gridBounds.top) / gridBounds.height());
	if (x2 < 0) x2 = 0; else if (x2 > geomGridX-1) x2 = geomGridX-1;
	if (z2 < 0) z2 = 0; else if (z2 > geomGridZ-1) z2 = geomGridZ-1;

	//Preload the grid blocks
	for (int x = x1; x <= x2; ++x) {
		for (int z = z1; z <= z2; ++z) {
			GeometryPage *page = _getGridPage(x, z);

			//If the page isn't loaded
			if (!page->_loaded){
				//Load the geometry immediately
				_loadPage(page);
				loadedList.push_back(page);
				page->_iter = (--loadedList.end());

				//And remove it from the pending list if necessary
				if (page->_pending){
					pendingList.remove(page);
					page->_pending = false;
				}
			}

			//Flag the page so it won't expire and be deleted in a few seconds if
			//it's currently not in the viewing range.
			page->_keepLoaded = true;
		}
	}
}

void GeometryPageManager::resetPreloadedGeometry(){
	//Set all grid blocks' _keepLoaded flag to false
	//WARNING:(possible bug? should be false or true?)
	for (int x = 0; x < geomGridX; ++x) {
		for (int z = 0; z < geomGridZ; ++z) {
			GeometryPage *page = _getGridPage(x, z);
			page->_keepLoaded = true;
		}
	}
}


//Loads the given page of geometry immediately
//Note: _loadPage() does add the page to loadedList, so that will have to be done manually
void GeometryPageManager::_loadPage(GeometryPage *page){
	//Calculate page info
	PageInfo info;
	float halfPageSize = mainGeom->getPageSize() * 0.5f;

	info.bounds.left = page->_centerPoint.get_x() - halfPageSize;
	info.bounds.right = page->_centerPoint.get_x() + halfPageSize;
	info.bounds.top = page->_centerPoint.get_y() - halfPageSize;
	info.bounds.bottom = page->_centerPoint.get_y() + halfPageSize;
	info.centerPoint = page->_centerPoint;
	info.xIndex = page->_xIndex;
	info.zIndex = page->_zIndex;
	info.userData = page->_userData;

	//Check if page needs unloading (if a delayed unload has been issued)
	if (page->_needsUnload){
		page->removeEntities();
		mainGeom->getPageLoader()->unloadPage(info);
		page->_userData = 0;
		page->_needsUnload = false;

		page->clearBoundingBox();
	}

	//Load the page
	page->setRegion(info.bounds.left, info.bounds.top, info.bounds.right, info.bounds.bottom);

	mainGeom->getPageLoader()->geomPage = page;
	mainGeom->getPageLoader()->loadPage(info);

	page->_userData = info.userData;

	page->build();
	page->setVisible(page->_visible);

	page->_inactiveTime = 0;
	page->_loaded = true;
	page->_fadeEnable = false;
}

//Unloads the given page of geometry immediately
//Note: _unloadPage() does not remove the page from loadedList, so that will have to be done manually
void GeometryPageManager::_unloadPage(GeometryPage *page){
	//Calculate boundaries to unload
	PageInfo info;
	float halfPageSize = mainGeom->getPageSize() * 0.5f;

	info.bounds.left = page->_centerPoint.get_x() - halfPageSize;
	info.bounds.right = page->_centerPoint.get_x() + halfPageSize;
	info.bounds.top = page->_centerPoint.get_y() - halfPageSize;
	info.bounds.bottom = page->_centerPoint.get_y() + halfPageSize;
	info.centerPoint = page->_centerPoint;
	info.xIndex = page->_xIndex;
	info.zIndex = page->_zIndex;
	info.userData = page->_userData;

	//Unload the page
	page->removeEntities();
	mainGeom->getPageLoader()->unloadPage(info);
	page->_userData = 0;
	page->_needsUnload = false;

	page->clearBoundingBox();

	page->_inactiveTime = 0;
	page->_loaded = false;
	page->_fadeEnable = false;
}

//"Virtually" unloads the given page of geometry. In reality it is unloaded during the next load.
//Note: _unloadPageDelayed() does not remove the page from loadedList, so that will have to be done manually
void GeometryPageManager::_unloadPageDelayed(GeometryPage *page){
	page->_needsUnload = true;
	page->_loaded = false;
}


//Scrolls pages in the grid by the given amount
void GeometryPageManager::_scrollGridPages(int shiftX, int shiftZ){
	//Check if the camera moved completely out of the grid
	if (shiftX > geomGridX || shiftX < -geomGridX || shiftZ > geomGridZ || shiftZ < -geomGridZ){
		//If so, just reload all the tiles (reloading is accomplished by unloading - loading is automatic)
		for (int x = 0; x < geomGridX; ++x){
			for (int z = 0; z < geomGridZ; ++z){
				GeometryPage *page = _getGridPage(x, z);
				if (page->_loaded){
					page->_keepLoaded = false;
					_unloadPage(page);
					loadedList.erase(page->_iter);
				}
				page->_centerPoint.set_x(page->_centerPoint.get_x() + shiftX * mainGeom->getPageSize());
				page->_centerPoint.set_y(page->_centerPoint.get_y() + shiftZ * mainGeom->getPageSize());
				page->_xIndex += shiftX;
				page->_zIndex += shiftZ;
			}
		}
	} else { //If not, scroll the grid by the X and Y values
		//Scroll horizontally (X)
		if (shiftX > 0){
			for (int z = 0; z < geomGridZ; ++z){
				//Temporarily store off-shifted pages first
				for (int x = 0; x < shiftX; ++x){
					GeometryPage *page = _getGridPage(x, z);
					if (page->_loaded){
						page->_keepLoaded = false;
						_unloadPageDelayed(page);
						loadedList.erase(page->_iter);
					}
					scrollBuffer[x] = page;
				}
				//Shift left
				int shiftedMidpoint = geomGridX - shiftX;
				for (int x = 0; x < shiftedMidpoint; ++x)
					_setGridPage(x, z, _getGridPage(x + shiftX, z));
				//Rotate temporary pages around to other side of grid
				for (int x = 0; x < shiftX; ++x){
					scrollBuffer[x]->_centerPoint.set_x(scrollBuffer[x]->_centerPoint.get_x() + geomGridX * mainGeom->getPageSize());
					scrollBuffer[x]->_xIndex += geomGridX;
					_setGridPage(x + shiftedMidpoint, z, scrollBuffer[x]);
				}
			}
		}
		else if (shiftX < 0) {
			for (int z = 0; z < geomGridZ; ++z){
				//Temporarily store off-shifted pages first
				int initialMidpoint = geomGridX + shiftX;
				for (int x = initialMidpoint; x < geomGridX; ++x){
					GeometryPage *page = _getGridPage(x, z);
					if (page->_loaded){
						page->_keepLoaded = false;
						_unloadPageDelayed(page);
						loadedList.erase(page->_iter);
					}
					scrollBuffer[x - initialMidpoint] = page;
				}
				//Shift right
				for (int x = geomGridX-1; x >= -shiftX; x--)
					_setGridPage(x, z, _getGridPage(x + shiftX, z));
				//Rotate temporary pages around to other side of grid
				for (int x = 0; x < -shiftX; ++x){
					scrollBuffer[x]->_centerPoint.set_x(scrollBuffer[x]->_centerPoint.get_x() - geomGridX * mainGeom->getPageSize());
					scrollBuffer[x]->_xIndex -= geomGridX;
					_setGridPage(x, z, scrollBuffer[x]);
				}
			}
		}
		//Scroll vertically (Z)
		if (shiftZ > 0){
			for (int x = 0; x < geomGridX; ++x){
				//Temporarily store off-shifted pages first
				for (int z = 0; z < shiftZ; ++z){
					GeometryPage *page = _getGridPage(x, z);
					if (page->_loaded){
						page->_keepLoaded = false;
						_unloadPageDelayed(page);
						loadedList.erase(page->_iter);
					}
					scrollBuffer[z] = page;
				}
				//Shift left
				int shiftedMidpoint = geomGridZ - shiftZ;
				for (int z = 0; z < shiftedMidpoint; ++z)
					_setGridPage(x, z, _getGridPage(x, z + shiftZ));
				//Rotate temporary pages around to other side of grid
				for (int z = 0; z < shiftZ; ++z){
					scrollBuffer[z]->_centerPoint.set_y(scrollBuffer[z]->_centerPoint.get_y() + geomGridZ * mainGeom->getPageSize());
					scrollBuffer[z]->_zIndex += geomGridZ;
					_setGridPage(x, z + shiftedMidpoint, scrollBuffer[z]);
				}
			}
		}
		else if (shiftZ < 0) {
			for (int x = 0; x < geomGridX; ++x){
				//Temporarily store off-shifted pages first
				int initialMidpoint = geomGridZ + shiftZ;
				for (int z = initialMidpoint; z < geomGridZ; ++z){
					GeometryPage *page = _getGridPage(x, z);
					if (page->_loaded){
						page->_keepLoaded = false;
						_unloadPageDelayed(page);
						loadedList.erase(page->_iter);
					}
					scrollBuffer[z - initialMidpoint] = page;
				}
				//Shift right
				for (int z = geomGridZ-1; z >= -shiftZ; z--)
					_setGridPage(x, z, _getGridPage(x, z + shiftZ));
				//Rotate temporary pages around to other side of grid
				for (int z = 0; z < -shiftZ; ++z){
					scrollBuffer[z]->_centerPoint.set_y(scrollBuffer[z]->_centerPoint.get_y() - geomGridZ * mainGeom->getPageSize());
					scrollBuffer[z]->_zIndex -= geomGridZ;
					_setGridPage(x, z, scrollBuffer[z]);
				}
			}
		}
	}
}

