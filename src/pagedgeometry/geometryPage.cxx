#include "geometryPage.h"

GeometryPage::GeometryPage(){
	_visible = _fadeEnable = _pending = _pending = _loaded = _needsUnload = false;
	_trueBoundsUndefined = true;
	_trueBounds = new BoundingBox(LPoint3f(0), LPoint3f(0));
	_inactiveTime = 0; _xIndex = _zIndex = 0;
	_centerPoint = LPoint3f::zero();
	_userData = NULL;
	mHasQueryFlag = false;
	mQueryFlag = 0;
}

void GeometryPage::addEntityToBoundingBox(const NodePath &entity){
	//TODO: Maybe this is too heavy
	LPoint3f min, max;
	entity.calc_tight_bounds(min, max);
	PT(BoundingBox) entBounds = new BoundingBox(min, max);

	if(_trueBoundsUndefined) _trueBoundsUndefined = false;
	_trueBounds->extend_by(entBounds);
}


CPT(BoundingBox) GeometryPage::getBoundingBox(){
	return _trueBounds;
}

void GeometryPage::clearBoundingBox(){
	_trueBounds = new BoundingBox(LPoint3f(0), LPoint3f(0));
	_trueBoundsUndefined = true;
}
