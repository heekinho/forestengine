#ifndef PAGELOADER_H
#define PAGELOADER_H

#include "pandaFramework.h"
#include "tbounds.h"
#include "geometryPage.h"

struct PageInfo {
	TBounds bounds;
	LPoint3f centerPoint;

	int xIndex;
	int zIndex;

	void *userData;

	vector<NodePath> meshList;
};

class PageLoader {
public:
	virtual void loadPage(PageInfo &page) = 0;
	virtual void unloadPage(PageInfo &page) {}
	virtual void frameUpdate() {}
	virtual ~PageLoader() {}
protected:
	void addEntity(NodePath &entity);

	GeometryPage* getCurrentGeometryPage(){
		return geomPage;
	}
private:
	friend class GeometryPageManager;
	//Do NOT modify or use this variable - it is used internally by addEntity()
	GeometryPage *geomPage;
};

#endif
