#include "treeLoader3d.h"

#include "treeLoader3d.h"
#include "pagedGeometry.h"

TreeLoader3D::TreeLoader3D(PagedGeometry *geom, const TBounds &bounds){
	TreeLoader3D::geom = geom;
	pageSize = geom->getPageSize();
}

TreeLoader3D::~TreeLoader3D(){}

/*! Função auxiliar */
float random(float lower, float higher){
	return lower + (higher-lower)*(float(rand())/RAND_MAX);
}

void TreeLoader3D::addTree(const NodePath &entity){
	//treeList.push_back(entity);


	NodePath pagenp = NodePath("page");
	pagenp.attach_new_node(entity.node());

//	for(int i = 0; i < 100; i++){
//		NodePath bb = NodePath("bb");//treeList.at(0).copy_to(NodePath("test"));
//		entity.instance_to(bb);
//		bb.set_x(random(0, pageSize*2));
//		bb.set_z(random(0, pageSize*2));
//		bb.set_r(rand()%360);
//		//addEntity(bb);
//
//
//
//		bb.reparent_to(pagenp);
//	}
	//pagenp.clear_model_nodes();
	//pagenp.flatten_strong();
	//addEntity(pagenp);

	treeList.push_back(pagenp);
}


#include "cardMaker.h"

void TreeLoader3D::loadPage(PageInfo &page){
//	geom->getPageSize()
//	nout << "Loading Page: (" << page.xIndex << " : " << page.zIndex << ")" << endl;

	// Caching the batches
	// Maybe this should be in the page itself?
	if(batches.find(page.centerPoint) == batches.end()){
//		nout << "Building Page of Index: " << page.centerPoint << endl;
//		NodePath pagenp = NodePath("page");
//		for(int i = 0; i < 10; i++){
//			NodePath bb = treeList.at(0).copy_to(NodePath("test"));
//			treeList.at(0).instance_to(bb);
//			bb.set_x(random(page.bounds.left, page.bounds.right));
//			bb.set_y(random(page.bounds.bottom, page.bounds.top));
//			bb.set_z(0);
//			bb.set_h(bb, rand()%360);
//			//addEntity(bb);
//			bb.reparent_to(pagenp);
//		}
//		batches[page.centerPoint] = pagenp;



		CardMaker cm ("Cell");
		cm.set_frame(LVertex(page.bounds.left, page.bounds.bottom, 0),
					 LVertex(page.bounds.right, page.bounds.bottom, 0),
					 LVertex(page.bounds.right, page.bounds.top, 0),
					 LVertex(page.bounds.left, page.bounds.top, 0));
		cm.set_color(rand()/float(RAND_MAX), 0, 0, 1);
		NodePath cmnp = NodePath(cm.generate());
		cmnp.set_two_sided(true);
		batches[page.centerPoint] = cmnp;

	}
	addEntity(batches[page.centerPoint]);







//	NodePath teste = NodePath("teste");
//	treeList.at(0).instance_to(teste);
//	//teste.reparent_to(treeList.at(0).get_parent());
//	teste.set_pos(page.centerPoint);
//	addEntity(teste);

//	NodePath pagenp = NodePath("page");
//	for(int i = 0; i < 10; i++){
//		NodePath bb = treeList.at(0).copy_to(NodePath("test"));
//		treeList.at(0).instance_to(bb);
//		bb.set_x(random(page.bounds.left, page.bounds.right));
//		bb.set_y(random(page.bounds.bottom, page.bounds.top));
//		bb.set_z(0);
//		bb.set_h(bb, rand()%360);
//		//addEntity(bb);
//		bb.reparent_to(pagenp);
//	}
//	//pagenp.clear_model_nodes();
//	//pagenp.flatten_strong();
//	addEntity(pagenp);



//	NodePath setor = NodePath("setor");
//	treeList.at(0).instance_to(setor);
//	setor.set_pos(page.centerPoint);
//	cout << getCurrentGeometryPage() << " - " << getCurrentGeometryPage()->getQueryFlag() << endl;
//	if(getCurrentGeometryPage()->getQueryFlag() == 1){
//		setor.set_z(2);
//	}
//	addEntity(setor);


//	nout << page.xIndex << "-" << page.zIndex << endl;
//	for(int i = 0; i < treeList.size(); i++){
//		treeList.at(i).set_pos(page.centerPoint);
//		addEntity(treeList.at(i));
//	}
}
