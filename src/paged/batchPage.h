#ifndef BATCHPAGE_H
#define BATCHPAGE_H

#include "pagedGeometry.h"
#include "boundingBox.h"


class BatchPage: public GeometryPage {
public:
	BatchPage();
	virtual void init(PagedGeometry *geom, void* data);
	virtual ~BatchPage();

	virtual void addEntity(NodePath &entity);
	virtual void build();
	virtual void removeEntities();

	virtual void setVisible(bool visible);
	virtual void setFade(bool enabled, float visibleDist, float invisibleDist);

	virtual void addEntityToBoundingBox() {}
	virtual void clearBoundingBox() {}
	virtual CPT(BoundingBox) getBoundingBox();

protected :
	float visibleDist, invisibleDist;

	NodePath batch;
	PagedGeometry *geom;
};

#endif
