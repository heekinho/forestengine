#ifndef TREELOADER3D_H
#define TREELOADER3D_H


#include "pagedGeometry.h"


class TreeIterator3D;
class TreeIterator2D;

class TreeLoader3D: public PageLoader {
public:
	TreeLoader3D(PagedGeometry *geom, const TBounds &bounds);
	~TreeLoader3D();

	void addTree(const NodePath &entity);

	INLINE const TBounds &getBounds(){
		return actualBounds;
	}

	void loadPage(PageInfo &page);

private:
	//Information about the 2D grid of pages
	int pageGridX, pageGridZ;
	float pageSize;
	TBounds gridBounds, actualBounds;

	//Misc.
	PagedGeometry *geom;

	vector<NodePath> treeList;


	map<LPoint3f, NodePath> batches;
};

#endif
