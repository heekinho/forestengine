#include "batchPage.h"

BatchPage::BatchPage(){
	geom = NULL;
	batch = NodePath("batch");
}

CPT(BoundingBox) BatchPage::getBoundingBox(){
	LPoint3f min, max;
	batch.calc_tight_bounds(min, max);
	return new BoundingBox(min, max);
}


void BatchPage::init(PagedGeometry *_geom, void* data){
	geom = _geom;
}

BatchPage::~BatchPage(){
//	nout << "Destructing BatchPage" << endl;
	batch.remove_node();
}

void BatchPage::addEntity(NodePath &entity){
	entity.reparent_to(batch);
}

void BatchPage::build(){
//	nout << "BatchPage::build" << endl;
//	nout << batch.get_num_children() << endl;
//	batch.clear_model_nodes();
//	nout << batch.flatten_strong() << endl;
	batch.reparent_to(geom->getSceneNode());
}

void BatchPage::removeEntities(){
//	nout << "RemoveEntities" << endl;

	batch.remove_node();
	batch = NodePath("batch");
//	batch.hide();
}

void BatchPage::setVisible(bool visible){
	if(visible) batch.unstash();//batch.show();
	else batch.stash();//batch.hide();
}

void BatchPage::setFade(bool enabled, float visibleDist, float invisibleDist){
	this->visibleDist = visibleDist;
	this->invisibleDist = invisibleDist;
}
