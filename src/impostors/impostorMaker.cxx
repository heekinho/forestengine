/*
 * impostorMaker.cxx
 * Created on: 29/08/2012
 * Author: heekinho
 */

#include "impostorMaker.h"

#include "pnmImage.h"
#include "graphicsOutput.h"
#include "graphicsWindow.h"
#include "graphicsEngine.h"
#include "orthographicLens.h"

#include "impostor.h"


ImpostorMaker::ImpostorMaker() {
	// TODO Auto-generated constructor stub

}

ImpostorMaker::~ImpostorMaker() {
	// TODO Auto-generated destructor stub
}

/*
 */
PT(Impostor) ImpostorMaker::create_impostor(PT(GraphicsWindow) gwin, PT(SpriteSheet) spritesheet, int row, const NodePath &object){
	if(row > spritesheet->rows){
		nout << "No more space available on this spritesheet->" << "\n";
		return NULL;
	}

	/* Getting information about the size of each sprite */
	float sprite_width = ((float)spritesheet->width / (float)spritesheet->cols);
	float sprite_height = ((float)spritesheet->height / (float)spritesheet->rows);

	FrameBufferProperties fb = FrameBufferProperties::get_default();
	fb.set_alpha_bits(1);

	/* We now get buffer thats going to hold the texture of our new scene */
	PT(GraphicsOutput) buffer = gwin->make_texture_buffer(
			"spritebuffer", sprite_width, sprite_height, NULL, true, &fb);
	buffer->set_clear_color(LColorf(0, 0, 0, 0));

	/* Now we have to setup a new scene graph to make this scene */
	NodePath scene = NodePath("Scene");
	//scene.set_antialias(AntialiasAttrib::M_auto);

	/* This takes care of setting up ther camera properly */
	NodePath camera = _make_camera(buffer);
	camera.reparent_to(scene);

	/* Create a node to be rendered in the new scene, and put
	 * the model in a instanced way */
	NodePath subject = NodePath("Subject");
	object.instance_to(subject);

	/* Coloca o objeto para ser visualizado */
	subject.reparent_to(scene);

	/* We must calculate the aabb of the object (or subject)? */
	LPoint3 min, max;
	subject.calc_tight_bounds(min, max);

	/* Now we get the size and the center to get the camera correctly aligned
	 * when taking the shots and afterwards when showing the impostor */
	LVecBase3 bounds_size = max - min;
	LVecBase3 center = center = (min + max) * 0.5;
	float max_width = sqrt(pow(bounds_size[0], 2) + pow(bounds_size[1], 2));

	/* Lens configuration */
	PT(OrthographicLens) lens = new OrthographicLens();
	lens->set_film_size(max_width, bounds_size[2]);
	lens->set_near(0);
	lens->set_far(max_width * 2.0); //TODO: Configurar direito.
	DCAST(Camera, camera.node())->set_lens(lens);

	/* How much degrees from one shot to another? */
	float step = 360.0 / ((float) spritesheet->cols);

	for(int i = 0; i < spritesheet->cols; i++){
		/* For debug only purpose: */
		buffer->set_clear_color(LColorf(1, 1, 1, 0));
		//buffer->set_clear_color(LColorf(rand()/float(RAND_MAX), rand()/float(RAND_MAX), rand()/float(RAND_MAX), 1));

		/* Define the angle to take the object photo =) */
		float angle = i * step;

		/* Positioning the camera accordingly */
		camera.set_x(center.get_x() + max_width * cos(deg_2_rad(angle)));
		camera.set_y(center.get_y() + max_width * sin(deg_2_rad(angle)));
		camera.set_z(center.get_z());

		/* Setting the orientation of the camera to look to the center of the object */
		camera.look_at(center);

		/* Take the shot */
		buffer->get_engine()->render_frame();

		/* "Reveal the picture" =P */
		PNMImage image;
		buffer->get_texture(0)->store(image);

		/* Place the picture on the spritesheet */
		spritesheet->image.copy_sub_image(image, sprite_width * i, row * sprite_height,  0, 0, sprite_width, sprite_height);
	}

	/* Closes the buffer */
	buffer->get_engine()->remove_window(buffer);

	/* Generate and fill one impostor with the information */
	//Impostor* impostor = new Impostor();
	PT(Impostor) impostor = new Impostor();
	impostor->center = center;
	impostor->width = max_width;
	impostor->height = bounds_size[2];
	impostor->row = row;

	return impostor;
}


#include "partBundle.h"
#include "character.h"


PT(Impostor) ImpostorMaker::make_animated(PT(GraphicsWindow) gwin, PT(SpriteSheet) spritesheet,
		const NodePath &object, PT(AnimControl) anim, const string &anim_name){

	/* Getting information about the size of each sprite */
	float sprite_width = ((float)spritesheet->width / (float)spritesheet->cols);
	float sprite_height = ((float)spritesheet->height / (float)spritesheet->rows);

	FrameBufferProperties fb = FrameBufferProperties::get_default();
	fb.set_alpha_bits(1);

	/* We now get buffer thats going to hold the texture of our new scene */
	PT(GraphicsOutput) buffer = gwin->make_texture_buffer("AnimatedSpriteBuffer", sprite_width, sprite_height, NULL, true, &fb);
	buffer->set_clear_color(LColorf(0, 0, 0, 0));

	/* Now we have to setup a new scene graph to make this scene */
	NodePath scene = NodePath("Scene");
	//scene.set_antialias(AntialiasAttrib::M_auto);

	/* This takes care of setting up ther camera properly */
	NodePath camera = _make_camera(buffer);
	camera.reparent_to(scene);

	/* Get the teapot and rotates it for a simple animation */
	NodePath subject = NodePath("Subject");
	object.instance_to(subject);

	/* Calcula o maior size possível */
	//TODO: REVER ISSO AQUI!
	PT(Character) theChar = DCAST(Character, object.find("**/+Character").node());
//	if(theChar == NULL) return subject;

	LPoint3 min, max;
	object.calc_tight_bounds(min, max);

	for(int i = 0; i < anim->get_num_frames(); i++){
		LPoint3 cur_min, cur_max, cur_size;
		anim->pose(i);

		/* Force updating bounds */
	    for (int k = 0; k < theChar->get_num_bundles(); ++k){
	    	theChar->get_bundle(k)->force_update();
	    }

		object.calc_tight_bounds(cur_min, cur_max);

		/* Atualiza o min */
		if(cur_min[0] < min[0]) min[0] = cur_min[0];
		if(cur_min[1] < min[1]) min[1] = cur_min[1];
		if(cur_min[2] < min[2]) min[2] = cur_min[2];
		if(cur_max[0] > max[0]) max[0] = cur_max[0];
		if(cur_max[1] > max[1]) max[1] = cur_max[1];
		if(cur_max[2] > max[2]) max[2] = cur_max[2];

		nout << cur_min << ": " << cur_max << endl;
	}

	LVecBase3 bounds_size = max - min;
	LVecBase3 center = center = (min + max) * 0.5;
	float max_width = sqrt(pow(bounds_size[0], 2) + pow(bounds_size[1], 2));
	nout << max_width << endl;

	/* Lens configuration */
	PT(OrthographicLens) lens = new OrthographicLens();
	lens->set_film_size(max_width, bounds_size[2]);
	lens->set_near(0);
	lens->set_far(max_width * 2.0); // Configurar direito.
	DCAST(Camera, camera.node())->set_lens(lens);

	/* Coloca o objeto para ser visualizado */
	subject.reparent_to(scene);

	/* Renderiza a cena do buffer */
	//buffer->get_engine()->render_frame();

	/* How much degrees from one shot to another? */
	float step = 360.0 / ((float) spritesheet->cols);

	if(anim->get_num_frames() > spritesheet->rows){
		nout << "The frames don't fit in the spritesheet" << endl;
	}

	for(int j = 0; j < anim->get_num_frames(); j++){
		anim->pose(j);
//		buffer->set_clear_color(LColor(float(rand())/RAND_MAX, float(rand())/RAND_MAX, float(rand())/RAND_MAX, 1));
		for(int i = 0; i < spritesheet->cols; i++){
			float angle = i * step;

			camera.set_x(center.get_x() + max_width * cos(deg_2_rad(angle + 270.0)));
			camera.set_y(center.get_y() + max_width * sin(deg_2_rad(angle + 270.0)));
			camera.set_z(center.get_z());

			camera.look_at(center);

			/* Renderiza o frame e salva a imagem */
	//		char fname[20];
	//		sprintf(fname, "billboard-%i.png", i);
//			buffer->set_clear_color(LColor(float(rand())/RAND_MAX, float(rand())/RAND_MAX, float(rand())/RAND_MAX, 1));
			buffer->get_engine()->render_frame();
	//		_save_buffer(buffer, fname);

			PNMImage image;
			buffer->get_texture(0)->store(image);

			spritesheet->image.copy_sub_image(image, sprite_width * i, sprite_height * j, 0, 0, sprite_width, sprite_height);
		}
	}

	spritesheet->image.write("animated-sprite.png");

	/* Filtro de correção para o binary */
//	filter_binary_image(final_image);
	PT(Texture) texture = new Texture("AnimatedSprite");
	texture->load(spritesheet->image);

	/* Remove o buffer liberando a memória e processamento */
	buffer->get_engine()->remove_window(buffer);

	/* Generate and fill one impostor with the information */
	//Impostor* impostor = new Impostor();
	PT(Impostor) impostor = new Impostor();
	impostor->center = center;
	impostor->width = max_width;
	impostor->height = bounds_size[2];

	//TODO: Doesn't make sense
	impostor->row = 0;

	return impostor;
}


/*! Creates a camera and */
NodePath ImpostorMaker::_make_camera(PT(GraphicsOutput) win){
    PT(DisplayRegion) display_region = win->make_display_region();
	PT(Camera) camera = new Camera("cam");

	/* Configuração do Display Region */
	NodePath camera_np = NodePath(camera);
	display_region->set_camera(camera_np);

    return camera_np;
}

/*! Filter the image by expanding the color part of the image to eliminate
 *  some visual artifacts when binary alpha mode is enabled */
void ImpostorMaker::filter_binary_image(PNMImage &image, int num_passes){
	PNMImage altered_alpha = PNMImage(image.get_x_size(), image.get_y_size(), 2);
	altered_alpha.copy_channel(image, 3, 0);
	altered_alpha.copy_channel(image, 3, 1);


	for(int k = 0; k < num_passes; k++){
		for(int i = 1; i < image.get_x_size()-1; i++){
			for(int j = 1; j < image.get_y_size()-1; j++){
				LColord cur_xel = image.get_xel_a(i, j);
				if(cur_xel[3] < 0.5) {
					//image.set_xel_a(i, j, cur_xel);
					continue;
				}

				if(image.get_alpha(i-1, j) < 0.5){
					image.set_xel(i-1, j, cur_xel[0], cur_xel[1], cur_xel[2]);
					altered_alpha.set_channel(i-1, j, 0, 1.0);
				}

				if(image.get_alpha(i+1, j) < 0.5){
					image.set_xel(i+1, j, cur_xel[0], cur_xel[1], cur_xel[2]);
					altered_alpha.set_channel(i+1, j, 0, 1.0);
				}

				if(image.get_alpha(i, j-1)  < 0.5){
					image.set_xel(i, j-1, cur_xel[0], cur_xel[1], cur_xel[2]);
					altered_alpha.set_channel(i+1, j, 0, 1.0);
				}

				if(image.get_alpha(i, j+1)  < 0.5){
					image.set_xel(i, j+1, cur_xel[0], cur_xel[1], cur_xel[2]);
					altered_alpha.set_channel(i+1, j, 0, 1.0);
				}
			}
		}
		image.copy_channel(altered_alpha, 0, 3);
	}

	image.copy_channel(altered_alpha, 1, 3);
//	image.remove_alpha();
}
