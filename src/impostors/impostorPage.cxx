/*
 * impostorPage.cxx
 * Created on: 29/08/2012
 * Author: heekinho
 */

#include "impostorPage.h"

#include "meshDrawer.h"
#include "shader.h"
#include "shaderPool.h"
#include "texturePool.h"

#include "impostorManager.h"



void make_float_colors(NodePath &nodePath){
	PT(InternalName) color_name = InternalName::get_color();

	PT(GeomNode) gn = DCAST(GeomNode, nodePath.find("**/+GeomNode").node());
	for(int i = 0; i < gn->get_num_geoms(); i++){
		PT(Geom) geom = gn->modify_geom(i);
		CPT(GeomVertexData) vd = geom->get_vertex_data();


		CPT(GeomVertexFormat) format = vd->get_format();
		const GeomVertexColumn* color = format->get_column(color_name);

		if(color && color->get_numeric_type() == Geom::NT_float32){
			//nout << "Já tah float!" << endl;
		}
		else {
			PT(GeomVertexFormat) new_format_modify = new GeomVertexFormat(*format);
			new_format_modify->remove_column(color_name);

			PT(GeomVertexArrayFormat) color_array = new GeomVertexArrayFormat(color_name, 4, Geom::NT_float32, Geom::C_color);
			new_format_modify->add_array(color_array);
			CPT(GeomVertexFormat) new_format = GeomVertexFormat::register_format(new_format_modify);

			CPT(GeomVertexData) new_vd = vd->convert_to(new_format);
			if(new_vd){
				//nout << "consolidando" << endl;
				geom->set_vertex_data(new_vd);
			}
		}
	}
}


/*!
 * qtd: Quantity of billboards, ie. twice the triangle budget.
 */
ImpostorPage::ImpostorPage(int qtd, const NodePath &parent, PT(ImpostorManager) manager) {
	impostor_manager = manager;

	basic_drawer = new MeshDrawer();
	basic_drawer->set_budget(qtd * 2);

	/* Gets the NodePath which will have all geom */
	impostors = basic_drawer->get_root();
	impostors.reparent_to(parent);

	/* Apply the shader */
	CPT(Shader) generator_shader = ShaderPool::load_shader("billboard-static.sha");
//	CPT(Shader) generator_shader = ShaderPool::load_shader("billboard-animated.sha");
	impostors.set_shader(generator_shader);

	/* Configure the texture */
	PT(Texture) tex = impostor_manager->get_texture();
//	PT(Texture) tex = TexturePool::load_texture("forest.png");
//	tex->set_magfilter(Texture::FT_nearest);
//	tex->set_minfilter(Texture::FT_nearest);
	tex->set_magfilter(SamplerState::FT_linear);
	tex->set_minfilter(SamplerState::FT_linear);
//	tex->set_minfilter(Texture::FT_linear_mipmap_nearest);

//	tex->set_wrap_u(Texture::WM_mirror);
//	tex->set_wrap_v(Texture::WM_mirror);
	impostors.set_texture(tex);

	/* Set alpha clip type of transparency */
	impostors.set_transparency(TransparencyAttrib::M_binary);
//	impostors.set_transparency(TransparencyAttrib::M_alpha);
//	impostors.set_depth_write(false);
//	impostors.set_depth_test(false);
}

ImpostorPage::~ImpostorPage() {
	delete basic_drawer;
}


void ImpostorPage::generate(const NodePathCollection &nodes){
	/* We don't need this information */
	basic_drawer->begin(NodePath("camera"), NodePath("root"));

	NodePathCollection tagged_nodes = nodes.find_all_matches("**/=Impostor.name");
	for(int i = 0; i < tagged_nodes.size(); i++){
		NodePath node = tagged_nodes[i];
		string name = node.get_tag("Impostor.name");
		PT(Impostor) impostor = impostor_manager->get_impostor(name);
		draw_impostor(impostor, node.get_pos(parent), node.get_sx(parent), node.get_h(parent));
	}

	basic_drawer->end();

	make_float_colors(impostors);
}

/*! The pos should be in model space */
void ImpostorPage::draw_impostor(PT(Impostor) impostor, const LVector3 &pos, float scale, float orientation) {
	/* For now we don't want some transforms of the node. So we create a dummy */
	NodePath dummy = NodePath("dummy");
	dummy.set_pos_hpr_scale(pos, LVecBase3(orientation, 0, 0), LVecBase3(scale));

	/* Get the position all vertex will be initially (before the vertex shader) */
	LVector3 bpos = NodePath("").get_relative_point(dummy, impostor->center);

	/* Defines the frame which the vertex shader will use to offset
	 * the vertices, while keep facing the camera */
	LVector4 frame = LVector4(-impostor->width, impostor->width, -impostor->height, impostor->height) * 0.5 * scale;

	/* The informations coded in color are:
	 * r: Orientation of the billboard.
	 * g: Which line of the spritesheet is this model (the col in spritesheet is defined in shader, using orientation).
	 * b: ??
	 * a: ??
	 */
	LVector4 color = LColorf(orientation / 360.0, float(impostor->row) / 255.0, 5.0 / 256.0, 1.0);

	/* Here we draw all the vertices of the billboard at the center,
	 * passing several information to each vertex using the color column.
	 * In the shader, we retrieve this information
	 *
	 * TODO: If I make a quad here instead of two tris, I could reduce memory consumption by 33%, right? */
	basic_drawer->tri(
		bpos, color, LVector2(frame[0], frame[2]),
		bpos, color, LVector2(frame[1], frame[2]),
		bpos, color, LVector2(frame[1], frame[3])
	);
	basic_drawer->tri(
		bpos, color, LVector2(frame[1], frame[3]),
		bpos, color, LVector2(frame[0], frame[3]),
		bpos, color, LVector2(frame[0], frame[2])
	);
}
