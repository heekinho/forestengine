/*
 * impostorManager.h
 * Created on: 28/08/2012
 * Author: heekinho
 */

#ifndef IMPOSTORMANAGER_H
#define IMPOSTORMANAGER_H

#include "pvector.h"
#include "pmap.h"

#include "pointerTo.h"
#include "referenceCount.h"
#include "filename.h"

#include "impostor.h"

class Texture;

class ImpostorManager : public ReferenceCount {
public:
	ImpostorManager();
	virtual ~ImpostorManager();

	void load(const Filename &path);
//	void load_from_disk(const Filename &path);
	void save_to_disk(const Filename &path, const Filename &image_path);

	PT(Texture) get_texture();
	void build();


	void set_spritesheet(PT(SpriteSheet) spritesheet){
		this->spritesheet = spritesheet;
	}

	PT(SpriteSheet) get_spritesheet() const {
		return spritesheet;
	}


	void add_impostor(const string &name, PT(Impostor) impostor){
		impostors[name] = impostor;
	}

	PT(Impostor) get_impostor(const string &name) {
		if(impostors.find(name) == impostors.end())
			nout << "Impostor " << name << " not found" << "\n";
		return impostors[name];
	}

protected:
	PT(SpriteSheet) spritesheet;
	PT(Texture) texture;

	pmap<string, PT(Impostor)> impostors;
	typedef pmap<string, PT(Impostor)>::iterator ImpostorsIter;

private:

};

#endif /* IMPOSTORMANAGER_H */
