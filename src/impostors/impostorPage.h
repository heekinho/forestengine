/*
 * impostorPage.h
 * Created on: 29/08/2012
 * Author: heekinho
 */

#ifndef IMPOSTORPAGE_H
#define IMPOSTORPAGE_H

#include "luse.h"
#include "nodePath.h"
#include "nodePathCollection.h"

#include "impostor.h"

class MeshDrawer;
class Texture;
class Shader;

class ImpostorManager;


/*! Responsible for generating the structure and handling the shader to make heavy use
 *  of impostors.
 *
 *  TODO: Maybe should be better not using MeshDrawer. Look what columns it is creating.
 *  Take off any unnecessary data!!! IMPORTANT!
 *  Maybe use colors for colors and use the normal to write the infos!
 */
class ImpostorPage : public ReferenceCount {
public:
	ImpostorPage(int qtd, const NodePath &parent, PT(ImpostorManager) impostor_manager);
	virtual ~ImpostorPage();

	//void set_impostor_manager(PT(ImpostorManager) impostor_manager);


//	void generate(const NodePath &np);
	void generate(const NodePathCollection &nodes);

	inline NodePath get_impostors() const {
		return impostors;
	}

	void set_shader(CPT(Shader) shader);

	void draw_impostor(PT(Impostor) impostor, const LVector3 &pos, float scale, float orientation);

protected:
	MeshDrawer* basic_drawer;
	NodePath impostors;

	NodePath parent;

	PT(ImpostorManager) impostor_manager;
};

#endif /* IMPOSTORPAGE_H */
