/*
 * impostorMaker.h
 * Created on: 29/08/2012
 * Author: heekinho
 */

#ifndef IMPOSTORMAKER_H
#define IMPOSTORMAKER_H

#include "pointerTo.h"
#include "nodePath.h"
#include "pnmImage.h"

class Impostor;
class SpriteSheet;
class GraphicsOutput;
class GraphicsWindow;
class AnimControl;


/*! Responsible for making a impostor (billboard) from a 3d model */
class ImpostorMaker {
public:
	ImpostorMaker();
	virtual ~ImpostorMaker();

	static void filter_binary_image(PNMImage &image, int num_passes = 4);

	PT(Impostor) create_impostor(PT(GraphicsWindow) gwin, PT(SpriteSheet) spritesheet, int row, const NodePath &model);
	PT(Impostor) make_animated(PT(GraphicsWindow) gwin, PT(SpriteSheet) spritesheet, const NodePath &object, PT(AnimControl) anim, const string &anim_name);

private:
	NodePath _make_camera(PT(GraphicsOutput) win);
};

#endif /* IMPOSTORMAKER_H */
