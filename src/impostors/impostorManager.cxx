/*
 * impostorManager.cxx
 * Created on: 28/08/2012
 * Author: heekinho
 */

#include "impostorManager.h"

#include <iostream>
#include <istream>
#include <fstream>
#include <string>

#include "texture.h"
#include "virtualFile.h"
#include "virtualFileSimple.h"
#include "virtualFileSystem.h"


ImpostorManager::ImpostorManager() {

}

ImpostorManager::~ImpostorManager() {
	// TODO Auto-generated destructor stub
}


PT(Texture) ImpostorManager::get_texture(){
	return texture;
}

void ImpostorManager::build(){
	texture = new Texture();
	texture->load(spritesheet->image);
}


void ImpostorManager::load(const Filename &path){
	/* Open the file */
	VirtualFileSystem* vfs = VirtualFileSystem::get_global_ptr();
	istream &in = *vfs->open_read_file(path, true);
	if (in == (istream *) NULL) {
		nout << "Unable to open " << path << "\n";
		return;
	}

	string sbuffer;

	int used_slots;
	int width, height, rows, cols, channels;
	string spritesheet_path;

	/* Skips commentary, get the spritesheet path */
	while(in.peek() == '#' || in.peek() == '\n') getline(in, sbuffer);
	getline(in, spritesheet_path);
	in >> width >> height >> rows >> cols >> channels;
	getline(in, sbuffer); /* Consumes the rest of the line */

	/* Skips commentary, get the spritesheet path */
	while(in.peek() == '#' || in.peek() == '\n') getline(in, sbuffer);
	in >> used_slots;
	getline(in, sbuffer); /* Consumes the rest of the line */

	/* Get the names of the models */
	while(in.peek() == '#' || in.peek() == '\n') getline(in, sbuffer);
	for(int i = 0; i < used_slots; i++){
		getline(in, sbuffer);

		PT(Impostor) impostor = new Impostor();
		//in >> impostor->row;
		impostor->row = i;
		in >> impostor->center[0] >>  impostor->center[1] >>  impostor->center[2];
		in >> impostor->width >> impostor->height;

		add_impostor(sbuffer, impostor);


		getline(in, sbuffer); /* Consumes the rest of the line */

		/* Consumes commentaries and blanks */
		while(in.peek() == '#' || in.peek() == '\n') getline(in, sbuffer);
	}

	vfs->close_read_file(&in);

	/* Reads the texture */
	spritesheet = new SpriteSheet(width, height, rows, cols, channels);
	spritesheet->image.read(spritesheet_path);

	build();

	return;
}


void ImpostorManager::save_to_disk(const Filename &path, const Filename &image_path){
	if(!spritesheet) {
		nout << "Invalid Spritesheet " << path << "\n";
		return;
	}

	/* Open the file for writing */
	VirtualFileSystem* vfs = VirtualFileSystem::get_global_ptr();
	ostream &out = *vfs->open_write_file(path, true, true);
	if (out == (ostream *)NULL) {
		nout << "Unable to write " << path << "\n";
		return;
	}

	/* Writes the file */
	out << "# Spritesheet Info:" << "\n";
	out << image_path << "\n";
	out << spritesheet->width << " " << spritesheet->height << " ";
	out << spritesheet->rows << " " << spritesheet->cols << " ";
	out << spritesheet->image.get_num_channels() << "\n\n";

	out << "# Used slots:" << "\n";
	out << impostors.size() << "\n\n";

	for(ImpostorsIter it = impostors.begin(); it != impostors.end(); ++it){
		out << (*it).first << "\n";
		//out << (*it).second->row << "\n";
		out << (*it).second->center << "\n";
		out << (*it).second->width << " " << (*it).second->height << "\n";
		out << "\n";
	}

	spritesheet->image.write(image_path);

	vfs->close_write_file(&out);
}
