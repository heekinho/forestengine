/*
 * forest.h
 * Created on: 29/08/2012
 * Author: heekinho
 */

#ifndef FOREST_H
#define FOREST_H

#include "referenceCount.h"
#include "nodePathCollection.h"

class Forest : public ReferenceCount {
public:
	Forest();
	virtual ~Forest();

	static NodePath create_imposter_batch(const NodePathCollection &nps);
};

#endif /* FOREST_H */
