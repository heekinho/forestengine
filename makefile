PYTHONPATH = /usr/include/python2.7
PANDAPATHINCLUDE = /home/heekinho/panda3d/debug-build/include
PANDAPATHLIB = /home/heekinho/panda3d/debug-build/lib
INSTANCINGINCLUDE = /home/heekinho/GitRepositories/instancing-system/src

.SUFFIXES: .c .cpp .cxx .cc .h .hh


# Compiler Names:
C++		= 	g++
CC		= 	gcc
RANLIB  	= 	ranlib


# Flags for C++ compiler:
#CFLAGS			= 	-g -Wno-deprecated -fPIC -O2

CXXFLAGS 		= 	-g -Wno-deprecated -Wno-write-strings -fPIC # -O2

INCFLAGS 		= 	-I$(PYTHONPATH) \
					-I$(PANDAPATHINCLUDE) \
					-I/usr/include/eigen3 \
					-I$(INSTANCINGINCLUDE)

# Libraries to link with:
MATLIB			= 	-lm
GLLIBS			= 	-lGL -lGLU
GLUTLIB			= 	-lglut
GLUILIB			= 	-lglui
OMLIBS			=	-lOM_CORE -lOM_TOOLS	

LIBPANDA		= 	-lp3framework -lpanda -lpandafx -lpandaexpress -lp3dtoolconfig -lp3dtool -lp3pystub  -lpthread -linstancing

LIBPATH 		= 	-L$(PANDAPATHLIB)

LDFLAGS 		= 	$(LIBPANDA)


# Simdunas main program build rules

SUBDIRS_IMPOSTORS = src/impostors 
INCFLAGS_IMPOSTORS += $(patsubst %,-I%,$(SUBDIRS_IMPOSTORS))
SRC_IMPOSTORS := $(foreach dir,$(SUBDIRS_IMPOSTORS),$(wildcard $(dir)/*.cxx))
HEADERFILES_IMPOSTORS := $(foreach dir,$(SRC_IMPOSTORS),$(wildcard $(dir)/*.h))
OBJECTFILES_IMPOSTORS := $(patsubst src/%.cxx,src/%.o$,$(SRC_IMPOSTORS))

SUBDIRS = src src/util src/pagedgeometry src/paged
INCFLAGS += $(patsubst %,-I%,$(SUBDIRS))
SRC := $(foreach dir,$(SUBDIRS),$(wildcard $(dir)/*.cxx))
HEADERFILES := $(foreach dir,$(SUBDIRS),$(wildcard $(dir)/*.h))
OBJECTFILES := $(patsubst src/%.cxx,src/%.o$,$(SRC))

# Targets
all:	libimpostors.so forest-engine

forest-engine:	$(HEADERFILES) $(HEADERFILES_IMPOSTORS) $(OBJECTFILES)
		@echo "Linking ...."
		$(CXX) $(CXXFLAGS) $(LIBPATH) $(OBJECTFILES) $(LDFLAGS) -L. -limpostors -o forest-engine
		
libimpostors.so: $(HEADERFILES_IMPOSTORS) $(OBJECTFILES_IMPOSTORS)
		@echo "Making Library"
		#$(CXX) $(CPPFLAGS) $(LIBPATH) $(OBJECTFILES) $(LDFLAGS) -o forest-engine
		${CXX} ${CXXFLAGS} $(LIBPATH) $(LDFLAGS) $(OBJECTFILES_IMPOSTORS) -shared -fPIC -Wl,-soname,$@ -o $@
		
.cxx.o: $*.h
	@echo "Compiling C++ code ...."
	$(C++) -o $*.o -c $(CXXFLAGS) $(INCFLAGS) $(INCFLAGS_IMPOSTORS) $*.cxx

clean:
	@echo "Cleaning test ..."
	rm forest-engine $(OBJECTFILES) libimpostors.so $(OBJECTFILES_IMPOSTORS)

